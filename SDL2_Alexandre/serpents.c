#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//gcc serpents.c -o serpents -lSDL2 -Wall -Wextra -g

#define TAILLE 6
#define NOMBRE 10

void dessinrect(SDL_Renderer *renderer, int couleurs[NOMBRE][3], int tableau[NOMBRE][TAILLE][4])
{
    SDL_Rect rectangle;
    for (int k = 0; k < NOMBRE; k++)
    {
        SDL_SetRenderDrawColor(renderer, couleurs[k][0], couleurs[k][1], couleurs[k][2], 255);
        for (int i = 0; i < TAILLE; i++)
        {
            rectangle.x = tableau[k][i][0];
            rectangle.y = tableau[k][i][1];
            rectangle.w = tableau[k][i][2];
            rectangle.h = tableau[k][i][3];
            SDL_RenderDrawRect(renderer, &rectangle);
        }
    }
}

void traitement(int tableau[NOMBRE][TAILLE][4], int pos)
{
    int direction, sens;
    for (int k = 0; k < NOMBRE; k++)
    {
        direction = rand() % 2;
        sens = rand() % 2;
        if (!sens) sens=-1;
        tableau[k][(pos + 1) % TAILLE][direction] = tableau[k][pos][direction] + sens*tableau[k][pos][2+direction];
        tableau[k][(pos + 1) % TAILLE][(direction + 1) % 2] = tableau[k][pos][(direction + 1) % 2];
        tableau[k][(pos + 1) % TAILLE][2] = tableau[k][pos][2];
        tableau[k][(pos + 1) % TAILLE][3] = tableau[k][pos][3];
    }
}

void init(int tableau[NOMBRE][TAILLE][4], int width, int height) //[k][0 et 1] pos en x et en y, [k][2 et 3] hauteur et largeur
{
    for (int k = 0; k < NOMBRE; k++)
    {
        tableau[k][0][0] = rand() % width;
        tableau[k][0][1] = rand() % height;
        tableau[k][0][2] = rand() % 100;
        tableau[k][0][3] = rand() % 100;
        for (int i = 1; i < TAILLE; i++)
        {
            tableau[k][i][0] = 0;
            tableau[k][i][1] = 0;
            tableau[k][i][2] = 0;
            tableau[k][i][3] = 0;
        }
    }
    traitement(tableau, 0);
    traitement(tableau, 1);
}

void supercouleurs(int couleurs[NOMBRE][3])
{
    for (int k = 0; k < NOMBRE; k++)
    {
        couleurs[k][0] = rand() % 256;
        couleurs[k][1] = rand() % 256;
        couleurs[k][2] = rand() % 256;
    }
}

int main()
{
    Uint8 const *keystates;
    srand(time(0));
    SDL_Window *window;
    int width = 1200;
    int height = 800;
    window = SDL_CreateWindow("Les serpents du SWAG", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                              width, height,
                              SDL_WINDOW_RESIZABLE);
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    int running = 1;
    int pos = 2;
    int tableau[NOMBRE][TAILLE][4];
    init(tableau, width, height);
    int couleurs[NOMBRE][3];
    supercouleurs(couleurs);
    SDL_Event event;
    while (running)
    {
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_WINDOWEVENT:
                switch (event.window.event)
                {
                case SDL_WINDOWEVENT_CLOSE:
                    running = 0;
                    break;
                case SDL_WINDOWEVENT_SIZE_CHANGED:
                    width = event.window.data1;
                    height = event.window.data2;
                    break;
                }
                break;
            case SDL_QUIT:
                running = 0;
                break;
            }
            break;
        }
        keystates = SDL_GetKeyboardState(NULL);
        if (keystates[SDL_SCANCODE_ESCAPE])
        {
            running = 0;
        }
        if (keystates[SDL_SCANCODE_SPACE])
        {
            running = 0;
        }
        if (keystates[SDL_SCANCODE_Q])
        {
            running = 0;
        }
        traitement(tableau, pos);
        dessinrect(renderer, couleurs, tableau);
        pos = (pos + 1) % TAILLE;
        SDL_RenderPresent(renderer);
        SDL_Delay(200);
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}