#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//gcc rectangles_alea.c -o rectangles_alea -lSDL2 -Wall -Wextra -g

void dessinrect(SDL_Renderer *renderer, int posx, int posy, int taille)
{
    SDL_Rect rectangle;
    SDL_SetRenderDrawColor(renderer, rand() % 256, rand() % 256, rand() % 256, 255);
    rectangle.x = posx;
    rectangle.y = posy;
    rectangle.w = (rand() % taille) + 1;
    rectangle.h = (rand() % taille) + 1;
    SDL_RenderDrawRect(renderer, &rectangle);
}

int main()
{
    srand(time(0));
    SDL_Window *window;
    int width = 1200;
    int height = 800;
    window = SDL_CreateWindow("Les serpents du SWAG", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                              width, height,
                              SDL_WINDOW_RESIZABLE);
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    int running = 1;
    int max = 0;
    SDL_Event event;
    while (running)
    {
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_WINDOWEVENT:
                switch (event.window.event)
                {
                case SDL_WINDOWEVENT_CLOSE:
                    running = 0;
                    break;
                case SDL_WINDOWEVENT_SIZE_CHANGED:
                    width = event.window.data1;
                    height = event.window.data2;
                    break;
                }
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    running = 0;
                    break;
                case SDLK_SPACE:
                    running = 0;
                    break;
                case SDLK_q:
                    running = 0;
                    break;
                }
                break;
            case SDL_QUIT:
                running = 0;
                break;
            }
            break;
        }
        max = rand() % 100;
        for (int k = 0; k < max; k++)
        {
            dessinrect(renderer, rand() % width, rand() % height, 2*k + 1);
        }
        SDL_RenderPresent(renderer);
        SDL_Delay(200);
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}