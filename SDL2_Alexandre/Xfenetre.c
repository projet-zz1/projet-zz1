#include <SDL2/SDL.h>
#include <stdio.h>

//gcc Xfenetre.c -o Xfenetre -lSDL2 -Wall -Wextra -g 


SDL_Window *ouvrirfenetre(int x, int y)
{
    SDL_Window *window = SDL_CreateWindow("wow une fenêtre", x, y, 200, 200, 0);
    return window;
}

int main()
{
    SDL_Window *stock[30]; //normalement il en faut 28
    int c = 0;
    for (int k = 0; k < 1000; k = k + 70)
    {
        stock[c] = ouvrirfenetre(1.8 * k, 0.9 * k);
        c++;
    }
    for (int k = 0; k < 1000; k = k + 70)
    {
        stock[c] = ouvrirfenetre(1800 - (1.8 * k), 0.9 * k);
        c++;
    }
    SDL_Delay(4000);
    for (int k = c - 1; k > 0; k--)
    {
        SDL_DestroyWindow(stock[k]);
    }
    SDL_Quit();
    return 0;
}