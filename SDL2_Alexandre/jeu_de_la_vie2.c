#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//gcc jeu_de_la_vie2.c -o jeu_de_la_vie2 -lSDL2 -Wall -Wextra -g

#define N 30
#define M 40
const int SURVIE[9] = {0, 0, 1, 1, 0, 0, 0, 0, 0};
const int MORT[9] = {0, 0, 0, 1, 0, 0, 0, 0, 0};

void recopie(int grille[N][M], int nouvgrille[N][M])
{
    for (int k = 0; k < N; k++)
    {
        for (int i = 0; i < M; i++)
        {
            grille[k][i] = nouvgrille[k][i];
        }
    }
}

int somme_voisin(int grille[N][M], int k, int i)
{
    int c = 0;
    c = c + grille[((k - 1) + N) % N][((i - 1) + M) % M];
    c = c + grille[((k - 1) + N) % N][(i + M) % M];
    c = c + grille[((k - 1) + N) % N][((i + 1) + M) % M];
    c = c + grille[(k + N) % N][((i - 1) + M) % M];
    c = c + grille[(k + N) % N][((i + 1) + M) % M];
    c = c + grille[((k + 1) + N) % N][((i - 1) + M) % M];
    c = c + grille[((k + 1) + N) % N][(i + M) % M];
    c = c + grille[((k + 1) + N) % N][(i + 1 + M) % M];
    return c;
}

void bouge(int grille[N][M])
{
    int nouvgrille[N][M];
    int c;
    for (int k = 0; k < N; k++)
    {
        for (int i = 0; i < M; i++)
        {
            c = somme_voisin(grille, k, i);
            if (grille[k][i])
                nouvgrille[k][i] = SURVIE[c];
            else
                nouvgrille[k][i] = MORT[c];
        }
    }
    recopie(grille, nouvgrille);
}

void dessingrille(SDL_Renderer *renderer, int grille[N][M], int height, int width)
{
    SDL_Rect rectangle;
    int c;
    int largrect = width / N;
    int longrect = height / M;
    int couleur[2][3] = {{0, 0, 0}, {255, 255, 255}};
    for (int k = 0; k < N; k++)
    {
        for (int i = 0; i < M; i++)
        {
            c = grille[k][i];
            SDL_SetRenderDrawColor(renderer, couleur[c][0], couleur[c][1], couleur[c][2], 255);
            rectangle.x = i * longrect;
            rectangle.y = k * largrect;
            rectangle.w = longrect;
            rectangle.h = largrect;
            SDL_RenderFillRect(renderer, &rectangle);
        }
    }
}

void initialisergrille(int grille[N][M])
{
    srand(time(0));
    for (int k = 0; k < N; k++)
    {
        for (int i = 0; i < M; i++)
        {
            grille[k][i] = 0;
        }
    }
    int nb = rand() % N * M;
    for (int k = 0; k < nb; k++)
    {
        grille[rand() % N][rand() % M] = 1;
    }
}

int main()
{
    int grille[N][M];
    initialisergrille(grille);
    SDL_Window *window;
    int width = 1200;
    int height = 800;
    int pause = 0;
    window = SDL_CreateWindow("La belle vie qui coule dans nos veines", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                              width, height,
                              SDL_WINDOW_RESIZABLE);
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    int running = 1;
    int px, py;
    int changecase;
    int affiche = 0;
    SDL_Event event;
    while (running)
    {
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_WINDOWEVENT:
                switch (event.window.event)
                {
                case SDL_WINDOWEVENT_CLOSE:
                    running = 0;
                    break;
                case SDL_WINDOWEVENT_SIZE_CHANGED:
                    width = event.window.data1;
                    height = event.window.data2;
                    SDL_SetWindowSize(window, width, height);
                    SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
                    break;
                }
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_p:
                case SDLK_SPACE:
                    pause = (pause + 1) % 2;
                    break;
                case SDLK_ESCAPE:
                case SDLK_q:
                    running = 0;
                    break;
                }
                break;
            case SDL_MOUSEBUTTONDOWN:
                px = event.button.x;
                py = event.button.y;
                changecase = 0;
                if (SDL_GetMouseState(NULL, NULL)==SDL_BUTTON(SDL_BUTTON_LEFT))
                    changecase = 1;
                grille[py / (height / N)][px / (width / M)] = changecase;
                affiche = 4;
                break;
            case SDL_QUIT:
                running = 0;
                break;
            }
            break;
        }
        affiche++;

        if (affiche == 5)
        {
            affiche = 0;
            if (!pause)
            {
                bouge(grille);
            }
            dessingrille(renderer, grille, width, height);
            SDL_RenderPresent(renderer);
        }
        SDL_Delay(10);
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}