#include <SDL2/SDL.h>
#include <stdio.h>
#include <SDL2/SDL_image.h>

//gcc animation.c -o animation -lSDL2 -Wall -Wextra -g -lSDL2_image

void end_sdl(char ok,            // fin normale : ok = 0 ; anormale ok = 1
             char const *msg,    // message à afficher
             SDL_Window *window, // fenêtre à fermer
             SDL_Renderer *renderer)
{ // renderer à fermer
    char msg_formated[255];
    int l;

    if (!ok)
    {
        strncpy(msg_formated, msg, 250);
        l = strlen(msg_formated);
        strcpy(msg_formated + l, " : %s\n");

        SDL_Log(msg_formated, SDL_GetError());
    }

    if (renderer != NULL)
        SDL_DestroyRenderer(renderer);
    if (window != NULL)
        SDL_DestroyWindow(window);

    SDL_Quit();

    if (!ok)
    {
        exit(EXIT_FAILURE);
    }
}

void tableau_rouge(SDL_Texture *my_texture, SDL_Rect red_state[16])
{
    SDL_Rect source = {0}; // Rectangle définissant la zone totale de la planche

    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);

    /* Mais pourquoi prendre la totalité de l'image, on peut n'en afficher qu'un morceau, et changer de morceau :-) */

    int offset_x = source.w / 8; // La largeur d'une vignette de l'image, marche car la planche est bien réglée
    int offset_y = source.h / 4; // La hauteur d'une vignette de l'image, marche car la planche est bien réglée

    for (int k = 0; k < 16; k++)
    {
        red_state[k].w = offset_x;
        red_state[k].h = offset_y;
    }

    for (int k = 0; k < 8; k++)
    {
        red_state[k].x = (7 - k) * offset_x;
        red_state[k].y = 3 * offset_y;
    }

    for (int k = 8; k < 16; k++)
    {
        red_state[k].y = 0;
    }
    red_state[8].x = 0 * offset_x;
    red_state[9].x = 2 * offset_x;
    red_state[10].x = 7 * offset_x;
    red_state[11].x = 5 * offset_x;
    red_state[12].x = 4 * offset_x;
    red_state[13].x = 3 * offset_x;
    red_state[14].x = 2 * offset_x;
    red_state[15].x = 7 * offset_x;
}

void tableau_vert(SDL_Texture *my_texture, SDL_Rect green_state[16])
{
    SDL_Rect source = {0}; // Rectangle définissant la zone totale de la planche

    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);

    /* Mais pourquoi prendre la totalité de l'image, on peut n'en afficher qu'un morceau, et changer de morceau :-) */

    int offset_x = source.w / 8; // La largeur d'une vignette de l'image, marche car la planche est bien réglée
    int offset_y = source.h / 4; // La hauteur d'une vignette de l'image, marche car la planche est bien réglée

    for (int k = 0; k < 16; k++)
    {
        green_state[k].w = offset_x;
        green_state[k].h = offset_y;
    }

    for (int k = 0; k < 8; k++)
    {
        green_state[k].x = k * offset_x;
        green_state[k].y = 3 * offset_y;
    }
    for (int k = 8; k < 13; k++)
    {
        green_state[k].y = 0;
    }
    green_state[8].x = 7 * offset_x;
    green_state[9].x = 5 * offset_x;
    green_state[10].x = 0;
    green_state[11].x = 0;
    green_state[12].x = 0;
    green_state[13].x = 0;
    green_state[13].y = offset_y;
    green_state[14].x = 0;
    green_state[14].y = 2 * offset_y;
    green_state[15].x = offset_x;
    green_state[15].y = 2 * offset_y;
}

void animation(SDL_Texture *perso_rouge, SDL_Texture *perso_vert, SDL_Window *window, SDL_Renderer *renderer)
{
    SDL_Rect red_state[16];
    SDL_Rect green_state[16];
    tableau_rouge(perso_rouge, red_state);
    tableau_vert(perso_vert, green_state);
    SDL_Rect
        window_dimensions = {0}, // Rectangle définissant la fenêtre, on  n'utilisera que largeur et hauteur
        destination = {0};       // Rectangle définissant où la zone_source doit être déposée dans le renderer

    SDL_GetWindowSize(window, // Récupération des dimensions de la fenêtre
                      &window_dimensions.w,
                      &window_dimensions.h);
    window_dimensions.w = window_dimensions.w - 100;
    int speed = 40;
    float zoom = 3;
    for (int k = 0; k < 8; k++)
    {
        destination.x = window_dimensions.w - 3 * red_state[k].w - k * speed;
        destination.y = (window_dimensions.h - red_state[k].h) / 2;
        destination.w = (red_state[k].w) * zoom;
        destination.h = red_state[k].h * zoom;
        SDL_RenderCopy(renderer, perso_rouge, &red_state[k], &destination);
        destination.x = 100 + 3 * green_state[k].w + k * speed;
        SDL_RenderCopy(renderer, perso_vert, &green_state[k], &destination);
        SDL_RenderPresent(renderer);
        SDL_Delay(200);
        SDL_RenderClear(renderer);
    }
    destination.x = window_dimensions.w - 3 * red_state[8].w - 8 * speed - 20;
    destination.y = (window_dimensions.h - red_state[8].h) / 2;
    destination.w = (red_state[8].w) * zoom;
    destination.h = red_state[8].h * zoom;
    SDL_RenderCopy(renderer, perso_rouge, &red_state[8], &destination);
    destination.x = 100 + 3 * green_state[8].w + 8 * speed + 20;
    SDL_RenderCopy(renderer, perso_vert, &green_state[8], &destination);
    SDL_RenderPresent(renderer);
    SDL_Delay(200);
    SDL_RenderClear(renderer);

    destination.x = window_dimensions.w - 3 * red_state[9].w - 8 * speed - 30;
    destination.y = (window_dimensions.h - red_state[9].h) / 2;
    destination.w = (red_state[9].w) * zoom;
    destination.h = red_state[9].h * zoom;
    SDL_RenderCopy(renderer, perso_rouge, &red_state[9], &destination);
    destination.x = 100 + 3 * green_state[9].w + 8 * speed + 30;
    SDL_RenderCopy(renderer, perso_vert, &green_state[9], &destination);
    SDL_RenderPresent(renderer);
    SDL_Delay(200);
    SDL_RenderClear(renderer);

    destination.x = window_dimensions.w - 3 * red_state[10].w - 350;
    destination.y = (window_dimensions.h - red_state[10].h) / 2;
    destination.w = (red_state[10].w) * zoom;
    destination.h = red_state[10].h * zoom;
    SDL_RenderCopy(renderer, perso_rouge, &red_state[10], &destination);
    destination.x = 100 + 3 * green_state[10].w + 350;
    SDL_RenderCopy(renderer, perso_vert, &green_state[10], &destination);
    SDL_RenderPresent(renderer);
    SDL_Delay(600);
    SDL_RenderClear(renderer);

    for (int k = 11; k < 16; k++)
    {
        destination.x = window_dimensions.w - 3 * red_state[k].w - 350;
        destination.y = (window_dimensions.h - red_state[k].h) / 2;
        destination.w = (red_state[k].w) * zoom;
        destination.h = red_state[k].h * zoom;
        SDL_RenderCopy(renderer, perso_rouge, &red_state[k], &destination);
        destination.x = 100 + 3 * green_state[k].w + 350;
        SDL_RenderCopy(renderer, perso_vert, &green_state[k], &destination);
        SDL_RenderPresent(renderer);
        SDL_Delay(200);
        SDL_RenderClear(renderer);
    }
}

int main()
{
    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;

    SDL_DisplayMode screen;

    /*********************************************************************************************************************/
    /*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        end_sdl(0, "ERROR SDL INIT", window, renderer);

    SDL_GetCurrentDisplayMode(0, &screen);

    /* Création de la fenêtre */
    window = SDL_CreateWindow("La violence c'est mal",
                              SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED, screen.w * 0.66,
                              screen.h * 0.66,
                              SDL_WINDOW_OPENGL);
    if (window == NULL)
        end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

    /* Création du renderer */
    renderer = SDL_CreateRenderer(
        window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL)
        end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    SDL_Texture *perso_vert;
    SDL_Texture *perso_rouge;
    perso_vert = IMG_LoadTexture(renderer, "greenpants.png");
    perso_rouge = IMG_LoadTexture(renderer, "redpants.png");
    if (perso_vert == NULL)
        end_sdl(0, "Echec du chargement de l'image dans la texture", window, renderer);
    if (perso_rouge == NULL)
        end_sdl(0, "Echec du chargement de l'image dans la texture", window, renderer);
    animation(perso_rouge, perso_vert, window, renderer);
    SDL_DestroyTexture(perso_vert);
    IMG_Quit();
    SDL_DestroyTexture(perso_rouge);
    end_sdl(1, "Normal ending", window, renderer);
}