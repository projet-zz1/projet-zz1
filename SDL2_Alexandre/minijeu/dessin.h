#ifndef __DESSIN_H__
#define __DESSIN_H__

#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL_image.h>
#include "perso.h"
#include <SDL2/SDL_ttf.h>
#include <math.h>

#define ZOOM 0.085

int draw_player(SDL_Renderer *renderer, int last_pos, ship *player, SDL_Texture *my_texture[2][4],int boster);

void drawmaze(SDL_Texture *maze, SDL_Window *window, SDL_Renderer *renderer);

void dessinmap(SDL_Renderer *renderer, char *fichier);

void drawtime(int timer, SDL_Renderer *renderer, SDL_Window *window, TTF_Font *font1);

void victory(SDL_Renderer *renderer, TTF_Font *font1, int timer,SDL_Window *window);

void fail(SDL_Renderer *renderer, TTF_Font *font1, int timer,SDL_Window *window);

void drawbackground(SDL_Renderer *renderer,SDL_Texture* bg, float angle,SDL_Window* window);

void drawdeath(SDL_Renderer *renderer, ship *player, SDL_Texture *explosion, SDL_Texture *bg, SDL_Texture *maze, SDL_Window *window, float angle);


#endif