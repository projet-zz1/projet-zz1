#include "perso.h"

void bouge(ship *player, int inputx, int inputy)
{
    player->vx = player->vx + inputx;
    player->vy = player->vy + inputy;
    if (player->vx)
    {
        if (player->vx > 0)
            player->vx = player->vx - 1;
        else
            player->vx = player->vx + 1;
    }
    if (player->vy)
    {
        if (player->vy > 0)
            player->vy = player->vy - 1;
        else
            player->vy = player->vy + 1;
    }
    if (player->vx > VMAX)
        player->vx = VMAX;
    if (player->vx < -VMAX)
        player->vx = -VMAX;
    if (player->vy > VMAX)
        player->vy = VMAX;
    if (player->vy < -VMAX)
        player->vy = -VMAX;
    player->posx = player->posx + player->vx;
    player->posy = player->posy + player->vy;
}

void construct_grid(char *fichier, int grid[N][M])
{
    FILE *file = fopen(fichier, "r");
    if (file)
    {
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < M; j++)
            {
                grid[i][j] = 0;
            }
        }
        int stock;
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < M; j++)
            {
                fscanf(file, "%d", &stock);
                if (stock == 1)
                {
                    grid[i][j] = 1;
                    grid[i][j + 1] = 1;
                }
                if (stock == 2)
                {
                    grid[i][j] = 1;
                    grid[i + 1][j] = 1;
                }
                if (stock==3)
                {
                    grid[i][j]=2;
                }
            }
        }
    }
    fclose(file);
}

int collision(ship *player, int grid[N][M])
{
    int size=340*ZOOM;
    int death = 0;
    if (grid[player->posy/50][player->posx/50]==1)
    {
        death = 1;
    }
    if (grid[(player->posy + size)/50][player->posx/50]==1)
    {
        death = 1;
    }
    if (grid[player->posy/50][(player->posx + size)/50]==1)
    {
        death = 1;
    }
    if (grid[(player->posy + size)/50][(player->posx + size)/50]==1)
    {
        death = 1;
    }
        if (grid[player->posy/50][player->posx/50]==2)
    {
        death = 2;
    }
    if (grid[(player->posy + size)/50][player->posx/50]==2)
    {
        death = 2;
    }
    if (grid[player->posy/50][(player->posx + size)/50]==2)
    {
        death = 2;
    }
    if (grid[(player->posy + size)/50][(player->posx + size)/50]==2)
    {
        death = 2;
    }
    return death;
}