#include "main.h"

int main()
{
    Uint8 const *keystates;
    ship *player = malloc(sizeof(ship));
    player->posx = 60;
    player->posy = 700;
    player->vx = 0;
    player->vy = 0;
    int last_pos = 0;
    time_t begin = time(NULL);
    int timer;
    time_t current;

    if (SDL_Init(SDL_INIT_VIDEO) == -1)
    {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
        return EXIT_FAILURE;
    }

    SDL_Window *window;
    int width = 50*M;
    int height = 50*N;
    window = SDL_CreateWindow("L'ESPAAAAACE", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                              width, height,
                              SDL_WINDOW_RESIZABLE);

    if (window == 0)
    {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
        /* on peut aussi utiliser SDL_Log() */
    }

    SDL_Renderer *renderer;
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED); /*  SDL_RENDERER_SOFTWARE */
    if (renderer == 0)
    {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
    }

    SDL_Texture *my_texture[2][4];
    SDL_Texture *maze;
    maze=IMG_LoadTexture(renderer, "maze2.png");
    SDL_Texture *bg;
    bg=IMG_LoadTexture(renderer,"Nebula.png");
    SDL_Texture *explosion;
    explosion=IMG_LoadTexture(renderer,"explosion.png");
    my_texture[0][0] = IMG_LoadTexture(renderer, "haut.png");
    my_texture[0][1] = IMG_LoadTexture(renderer, "droite.png");
    my_texture[0][2] = IMG_LoadTexture(renderer, "bas.png");
    my_texture[0][3] = IMG_LoadTexture(renderer, "gauche.png");
    my_texture[1][0] = IMG_LoadTexture(renderer, "hautboster.png");
    my_texture[1][1] = IMG_LoadTexture(renderer, "droiteboster.png");
    my_texture[1][2] = IMG_LoadTexture(renderer, "basboster.png");
    my_texture[1][3] = IMG_LoadTexture(renderer, "gaucheboster.png");
    //dessinmap(renderer,"niveau.txt"); A appeler uniquement pour la génération du graphisme du labyrinthe

    if (TTF_Init() < 0) printf("je refuse de fonctionner !!!!\n");
    TTF_Font *font1;
    font1= TTF_OpenFont("Arial.ttf", 30);
        if (font1 == NULL)
        printf("Can't load font\n");
    SDL_Event event;
    int running = 1;
    int inputx = 0;
    int inputy = 0;
    float angle=0;
    int boster=0;
    int touch=0;
    int grid[N][M];
    construct_grid("niveau.txt",grid);

    while (running)
    {
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT:
                running = 0;
                break;
            case SDL_WINDOWEVENT:
                switch (event.window.event)
                {
                case SDL_WINDOWEVENT_CLOSE:
                    running = 0;
                    break;
                case SDL_WINDOWEVENT_SIZE_CHANGED:
                    width = event.window.data1;
                    height = event.window.data2;
                    break;
                }
                break;
            }
            break;
        }
        keystates = SDL_GetKeyboardState(NULL);
        if (keystates[SDL_SCANCODE_LEFT])
        {
            inputx = inputx - ACCELERATION;
            boster=1;
        }
        if (keystates[SDL_SCANCODE_RIGHT])
        {
            inputx = inputx + ACCELERATION;
            boster=1;
        }
        if (keystates[SDL_SCANCODE_UP])
        {
            inputy = inputy - ACCELERATION;
            boster=1;
        }
        if (keystates[SDL_SCANCODE_DOWN])
        {
            inputy = inputy + ACCELERATION;
            boster=1;
        }
        if (keystates[SDL_SCANCODE_ESCAPE])
        {
            running = 0;
        }
        bouge(player, inputx, inputy);
        touch=collision(player,grid);
        if (touch==1)
        {
            drawdeath(renderer,player,explosion,bg,maze,window,angle);
            running=0;
            fail(renderer,font1,timer,window);
        }
        if (touch==2)
        {
            running=0;
            victory(renderer,font1,timer,window);
        }

        inputx=0;
        inputy=0;
        angle=angle+(2*M_PI)/4000;
        current=time(NULL);
        timer=difftime(current,begin);
        drawbackground(renderer,bg,angle,window);
        drawmaze(maze,window,renderer);      
        last_pos = draw_player(renderer, last_pos, player, my_texture,boster);
        drawtime(timer,renderer,window,font1);
        boster=0;
        SDL_RenderPresent(renderer);
        SDL_Delay(15);
    }
    SDL_DestroyTexture(my_texture[0][0]);
    SDL_DestroyTexture(my_texture[0][1]);
    SDL_DestroyTexture(my_texture[0][2]);
    SDL_DestroyTexture(my_texture[0][3]);
    SDL_DestroyTexture(my_texture[1][0]);
    SDL_DestroyTexture(my_texture[1][1]);
    SDL_DestroyTexture(my_texture[1][2]);
    SDL_DestroyTexture(my_texture[1][3]);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    TTF_CloseFont(font1);
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();  
}