#include "dessin.h"

int draw_player(SDL_Renderer *renderer, int last_pos, ship *player, SDL_Texture *my_texture[2][4], int boster)
{ //0:haut    1:droite    2:bas   3:gauche
    if (player->vx == 0)
    {
        if (player->vy > 0)
            last_pos = 2;
        if (player->vy < 0)
            last_pos = 0;
    }
    else
    {
        if (player->vx > 0)
        {

            if (player->vx > abs(player->vy))
                last_pos = 1;
            if (player->vx < abs(player->vy))
            {
                if (player->vy > 0)
                    last_pos = 2;
                if (player->vy < 0)
                    last_pos = 0;
            }
        }
        else
        {
            if (abs(player->vx) > abs(player->vy))
                last_pos = 3;
            if (player->vx < abs(player->vy))
            {
                if (player->vy > 0)
                    last_pos = 2;
                if (player->vy < 0)
                    last_pos = 0;
            }
        }
    }
    SDL_Rect destination = {0};
    SDL_Rect cadre = {0};
    destination.x = player->posx;
    destination.y = player->posy;
    SDL_QueryTexture(my_texture[boster][last_pos], NULL, NULL, &destination.w, &destination.h);
    SDL_QueryTexture(my_texture[boster][last_pos], NULL, NULL, &cadre.w, &cadre.h);
    destination.w = destination.w * ZOOM;
    destination.h = destination.h * ZOOM;
    SDL_RenderCopy(renderer, my_texture[boster][last_pos], &cadre, &destination);
    return last_pos;
}

void drawmaze(SDL_Texture *maze, SDL_Window *window, SDL_Renderer *renderer)
{
    SDL_Rect
        source = {0},
        destination = {0};

    SDL_GetWindowSize(window, &destination.w, &destination.h);
    SDL_QueryTexture(maze, NULL, NULL, &source.w, &source.h);

    SDL_RenderCopy(renderer, maze, &source, &destination);
}

void dessinmap(SDL_Renderer *renderer, char *fichier)
{
    SDL_Texture *wallv;
    SDL_Texture *wallh;
    wallv = IMG_LoadTexture(renderer, "wallvertical.png");
    wallh = IMG_LoadTexture(renderer, "wallhorizon.png");
    SDL_Rect destination = {0}, source = {0};
    SDL_Rect cadre = {0};
    SDL_QueryTexture(wallv, NULL, NULL, &source.w, &source.h);
    int offsetx = source.w / 4;
    int offsety = source.h / 4;
    FILE *file = fopen(fichier, "r");
    int stock;
    if (file)
    {
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < M; j++)
            {
                fscanf(file, "%d", &stock);
                if (stock == 1) //mur horizontal vert
                {
                    destination.x = j * 50;
                    destination.y = i * 50;
                    destination.w = 100;
                    destination.h = 50;
                    cadre.y = 3 * offsety + 10;
                    cadre.x = 50;
                    cadre.w = 2 * offsetx - 68;
                    cadre.h = offsety - 55;
                    SDL_RenderCopy(renderer, wallh, &cadre, &destination);
                }
                if (stock == 2) //mur vertical vert
                {
                    destination.x = j * 50;
                    destination.y = i * 50;
                    destination.w = 50;
                    destination.h = 100;
                    cadre.x = 43;
                    cadre.y = 50;
                    cadre.w = offsetx - 50;
                    cadre.h = 2 * offsety - 68;
                    SDL_RenderCopy(renderer, wallv, &cadre, &destination);
                }
            }
        }
        fclose(file);
        SDL_DestroyTexture(wallh);
        SDL_DestroyTexture(wallv);
        SDL_RenderPresent(renderer);
        SDL_Delay(5000);
    }
}

void drawtime(int timer, SDL_Renderer *renderer, SDL_Window *window, TTF_Font *font1)
{

    SDL_Color color = {240, 255, 255, 255};
    char stock[20] = "";
    sprintf(stock, "Temps : %d s", timer);
    SDL_Surface *text_surface = NULL;
    text_surface = TTF_RenderText_Blended(font1, stock, color);
    SDL_Texture *text_texture = NULL;
    text_texture = SDL_CreateTextureFromSurface(renderer, text_surface);
    //if(!text_texture) printf("ahahahaa ça amrche pas\n");

    SDL_Rect pos = {0, 0, 0, 0};
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);
    SDL_GetWindowSize(window, &pos.x, NULL);
    pos.y = 0;
    pos.x = (pos.x - pos.w) / 2;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);
    SDL_DestroyTexture(text_texture);
    SDL_FreeSurface(text_surface);
}

void victory(SDL_Renderer *renderer, TTF_Font *font1, int timer, SDL_Window *window)
{
    SDL_Color color = {0, 0, 0, 255};
    SDL_RenderClear(renderer);

    color.r = 255;
    color.g = 131;
    char stock[40] = "";
    sprintf(stock, "BRAVO, vous avez gagne en %d s", timer);
    SDL_Surface *text_surface = NULL;
    text_surface = TTF_RenderText_Blended(font1, stock, color);
    SDL_Texture *text_texture = NULL;
    text_texture = SDL_CreateTextureFromSurface(renderer, text_surface);
    //if(!text_texture) printf("ahahahaa ça amrche pas\n");

    SDL_Rect pos = {0, 0, 0, 0};
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);
    SDL_GetWindowSize(window, &pos.x, &pos.y);
    pos.y = (pos.y - pos.h) / 2;
    pos.x = (pos.x - pos.w) / 2;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);
    SDL_RenderPresent(renderer);
    SDL_Delay(2000);
    SDL_DestroyTexture(text_texture);
    SDL_FreeSurface(text_surface);
}

void fail(SDL_Renderer *renderer, TTF_Font *font1, int timer, SDL_Window *window)
{
    SDL_Color color = {0, 0, 0, 255};
    SDL_RenderClear(renderer);

    color.r = 255;
    color.g = 131;
    char stock[50] = "";
    sprintf(stock, "DOMMAGE, vous avez perdu %d s de votre vie", timer);
    SDL_Surface *text_surface = NULL;
    text_surface = TTF_RenderText_Blended(font1, stock, color);
    SDL_Texture *text_texture = NULL;
    text_texture = SDL_CreateTextureFromSurface(renderer, text_surface);
    //if(!text_texture) printf("ahahahaa ça amrche pas\n");

    SDL_Rect pos = {0, 0, 0, 0};
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);
    SDL_GetWindowSize(window, &pos.x, &pos.y);
    pos.y = (pos.y - pos.h) / 2;
    pos.x = (pos.x - pos.w) / 2;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);
    SDL_RenderPresent(renderer);
    SDL_Delay(2000);
    SDL_DestroyTexture(text_texture);
    SDL_FreeSurface(text_surface);
}

void drawbackground(SDL_Renderer *renderer, SDL_Texture *bg, float angle, SDL_Window *window)
{
    SDL_Rect destination = {0}, source = {0};
    SDL_GetWindowSize(window, &destination.w, &destination.h);
    source.x = 200 + 1000 * cos(angle);
    source.y = 200 + 1550 * sin(angle);
    source.h = destination.h;
    source.w = destination.w;
    SDL_RenderCopy(renderer, bg, &source, &destination);
}

void drawdeath(SDL_Renderer *renderer, ship *player, SDL_Texture *explosion, SDL_Texture *bg, SDL_Texture *maze, SDL_Window *window, float angle)
{
    SDL_Rect source = {0}, destination = {0};

    SDL_QueryTexture(explosion, NULL, NULL, &source.w, &source.h);
    int offsetx = source.w / 4;
    int offsety = source.h / 4;

    float zoom=0.8;
    source.w=offsetx;
    source.h=offsety;
    destination.w = source.w*zoom;
    destination.h = source.h*zoom;
    destination.x = player->posx;
    destination.y = player->posy;

    for (int k = 0; k < 4; k++)
    {
        for (int i = 0; i < 4; i++)
        {
            drawbackground(renderer, bg, angle, window);
            drawmaze(maze, window, renderer);
            source.x = i * offsetx;
            source.y = k * offsety;
            SDL_RenderCopy(renderer, explosion, &source, &destination);
            SDL_RenderPresent(renderer);
            SDL_Delay(30);
        }
    }
}