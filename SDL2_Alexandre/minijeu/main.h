#ifndef __MAIN_H__
#define __MAIN_H__

#include <SDL2/SDL.h>
#include <stdio.h>
#include <SDL2/SDL_image.h>
#include "perso.h"
#include "dessin.h"
#include <time.h>
#include <SDL2/SDL_ttf.h>
#include <math.h>

#define ACCELERATION 2

int main();

#endif