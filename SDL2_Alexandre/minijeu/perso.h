#ifndef __PERSO_H__
#define __PERSO_H__

#include <stdio.h>
#include <stdlib.h>

typedef struct vaisseau{
    int posx;
    int posy;
    int vx;
    int vy;
}ship;

#define VMAX 15
#define N 16
#define M 20
#define ZOOM 0.085

void bouge(ship* player, int inputx, int inputy);

void construct_grid(char *fichier, int grid[N][M]);

int collision(ship* player,int grid[N][M]);

/*char const *maze =
#include "data.txt"
;
"   xxxxxxx   x x x  xxx     xxx  x"
"           xxx   xxxxxxxxxx     xx"
"xx xx   xxx                       "
;

"Hello" " " "World"
"!"

"Hello World!"*/
#endif