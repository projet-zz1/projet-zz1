#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "CodeTasBinaire.h"
#include "CodeTableau.h"
#include "CodeIntSpecial.h"

#define HAUTEUR_MAX 10
#define TAILLE_MAX (ExpoRapide(2 , HAUTEUR_MAX) + 1)


int ProfondeurNoeud(int i0)
{
    int profondeur = 1;
    int i = i0;

    while (i > 1)
    {
        i = i/2;
        profondeur++;
    }

    return(profondeur);
}

int HauteurTas(int tas[])
{
    return(ProfondeurNoeud(tas[0]));
}


void PercolateUp(int tas[] , int i0)
{
    int profondeur;
    int i = i0;
    int ind_pere;
    int fin = 0;

    profondeur = ProfondeurNoeud(i0);

    while ((profondeur > 1) && (! fin))
    {
        ind_pere = i / 2;
        if (tas[i] < tas[ind_pere])
        {
            Permutation(tas , i , ind_pere);
            i = ind_pere;
            profondeur--;
        }
        else
        {
            fin = 1;
        }
    }
}

void PercolateDown(int tas[] , int i0)
{
    int profondeur;
    int ind_min;
    int i = i0;
    int hauteur = HauteurTas(tas);
    int fin = 0;

    profondeur = ProfondeurNoeud(i0);    

    while ((profondeur < hauteur) && (! fin))
    {
        if (2 * i <= tas[0])
        {
            ind_min = 2 * i;
            if (ind_min + 1 <= tas[0])
            {
                ind_min = IndMinimum(tas , ind_min , ind_min + 1);
            }

            if (tas[i] > tas[ind_min])
            {
                Permutation(tas , i , ind_min);
                i = ind_min;
                profondeur++;
            }
            else
            {
                fin = 1;
            }
        }
        else
        {
            fin = 1;
        }
    }
}

void ConstructionTas(int tas[] , int T[] , int length)
{
    int i;
    int hauteur;

    tas[0] = length;
    TabCopie(T , tas , 0 , length-1 , 1);

    hauteur = HauteurTas(tas);
    for (i= ExpoRapide(2 , (hauteur-1)) ; i>0 ; i--)
    {
        PercolateDown(tas , i);
    }
}

int AjouterTas(int Tas[] , int val , int taille_tas)
{
    int erreur = 0;

    if (Tas[0] < taille_tas)
    {
        Tas[0]++;
        Tas[Tas[0]] = val;
        PercolateUp(Tas , Tas[0]);
    }
    else
    {
        erreur = 1;
    }

    return(erreur);
}

int RetirerTas(int Tas[])
{
    int erreur = 0;

    if (Tas[0] > 0)
    {
        Tas[1] = Tas[Tas[0]];
        Tas[0]--;
        PercolateDown(Tas , 1);
    }
    else
    {
        erreur = 1;
    }

    return(erreur);
}

void AffichageTas(int Tas[])
{
    int i;
    int k = 1;

    for (i=1 ; i<=Tas[0]  ; i++)
    {
        printf("%d\t" , Tas[i]);
        if (i == k)
        {
            printf("\n");
            k = k * 2 + 1;
        }
    }
    printf("\n\n");
}

void TriTasBinaire(int T[] , int length)
{
    int Tas[TAILLE_MAX];
    int i;

    ConstructionTas(Tas , T , length);

    for (i=0 ; i<length ; i++)
    {
        T[i] = Tas[1];
        RetirerTas(Tas); 
    }
}