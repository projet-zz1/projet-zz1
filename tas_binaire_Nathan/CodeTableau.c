#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "CodeTableau.h"


int IndMinimum(int T[] , int i1 , int i2)
{
    int ind_min = i1;
    
    if (T[i1] > T[i2])
    {
        ind_min = i2;
    }
    return(ind_min);
}

void Permutation(int T[] , int i1 , int i2)
{
    int temp = T[i1];

    T [i1] = T[i2];
    T[i2] = temp;
}

void TabCopie(int Source[] , int Destination[] , int i0 , int i_fin , int j0)
{
    int i;
    
    for (i=i0 ; i<=i_fin ; i++)
    {
        Destination[j0 + i] = Source[i];
    }

}

void AffichageTab(int T[] , int length)
{
    int i;

    printf("\nT = {");
    for (i=0 ; i<length-1 ; i++)
    {
        printf("%d ; " , T[i]);
    }
    printf("%d}\n" , T[length-1]);
}

void RandomTab(int T[] , int length)
{
    int i;
    int  germe = time(0);

    srand(germe);

    for (i=0 ; i<length ; i++)
    {
        T[i] = rand();
    }
}