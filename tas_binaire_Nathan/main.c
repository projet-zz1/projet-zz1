#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "main.h"
#include "CodeTasBinaire.h"
#include "CodeTableau.h"
#include "CodeIntSpecial.h"

#define HAUTEUR_MAX 10
#define TAILLE_MAX (ExpoRapide(2 , HAUTEUR_MAX) + 1)


int main()
{
    int Tas[TAILLE_MAX];
    int T[] = {31 , 36 , 41 , 53 , 30 , 6 , 21 , 28 , 16 , 20 , 100 , 1024 , 5 , 18 , 16 , 54 , 78 , 91 , 13};
    int T1[1000];
    int T2[1000];
    clock_t t1_init , t1_fin , t2_init , t2_fin;


    AffichageTab(T , 19);
    ConstructionTas(Tas , T , 19);

    AffichageTas(Tas);
    printf("La hauteur du tas est %d\n" , HauteurTas(Tas));
    AjouterTas(Tas , 23 , TAILLE_MAX);
    printf("apres insertion de 23, le tas est\n\n");
    AffichageTas(Tas);
    AjouterTas(Tas , 8 , TAILLE_MAX);
    printf("apres insertion de 8, le tas est\n\n");
    AffichageTas(Tas);


    RandomTab(T1 , 1000);
    RandomTab(T2 , 1000);

    t1_init = clock();
    TriTasBinaire(T1 , 1000);
    t1_fin = clock();

    t2_init = clock();
    qsort(T2 , 1000 , sizeof(int) , intComparator);
    t2_fin = clock();

    printf("Temps par TriTasBinaire = %ld\n" , (t1_fin - t1_init));
    printf("Temps par Quicksort = %ld\n" , (t2_fin - t2_init));
    

    return 0;
}