#ifndef TABLEAU_H
#define TABLEAU_H


int IndMinimum(int T[] , int i1 , int i2);

void Permutation(int T[] , int i1 , int i2);

void TabCopie(int Source[] , int Destination[] , int i0 , int i_fin , int j0);

void AffichageTab(int T[] , int length);

void RandomTab(int T[] , int length);

#endif