#include "CodeIntSpecial.h"


int ExpoRapide(int a , int b)
{
    int reste = 1;
    int resultat = a;
    int puissance = b;

    while (puissance > 1)
    {
        if (puissance % 2)
        {
            reste = reste * resultat;
        }
        resultat = resultat * resultat;
        puissance = puissance / 2;
        
    }
    resultat = resultat * reste;

    return(resultat);
}

int intComparator(const void * first , const void * second)
{
    int firstInt = *(const int *)first;
    int secondInt = * (const int *)second;

    return(firstInt - secondInt);
}