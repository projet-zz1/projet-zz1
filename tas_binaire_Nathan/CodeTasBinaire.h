#ifndef TAS_BINAIRE_H
#define TAS_BINAIRE_H

//Le Tas est représenté par un tableau dont le premier element est le nombre d'elements du tas

int ProfondeurNoeud(int i0);

int HauteurTas(int tas[]);

void PercolateUp(int tas[] , int i0);

void PercolateDown(int tas[] , int i0);

void ConstructionTas(int * tas , int T[] , int length);

int AjouterTas(int Tas[] , int val , int taille_tas);

int RetirerTas(int Tas[]);

void AffichageTas(int Tas[]);

void TriTasBinaire(int T[] , int length);

#endif