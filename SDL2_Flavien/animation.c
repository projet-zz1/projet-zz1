#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

/*Compilation avec gcc animation.c -o animation -lSDL2 -lm -Wall -Wextra -g*/

void end_sdl(char ok, char const *msg, SDL_Window *window, SDL_Renderer *renderer)
{ 
  char msg_formated[255];
  int l;

  if (!ok)
  {
    strncpy(msg_formated, msg, 250);
    l = strlen(msg_formated);
    strcpy(msg_formated + l, " : %s\n");

    SDL_Log(msg_formated, SDL_GetError());
  }

  if (renderer != NULL)
    SDL_DestroyRenderer(renderer);
  if (window != NULL)
    SDL_DestroyWindow(window);

  SDL_Quit();

  if (!ok)
  {
    exit(EXIT_FAILURE);
  }
}

void draw(SDL_Renderer *renderer, int x, int y, int r, int v, int b)
{
  SDL_Rect rectangle2;

  SDL_SetRenderDrawColor(renderer, r, v, b, 255);

  rectangle2.x = x;
  rectangle2.y = y;
  rectangle2.w = 100;
  rectangle2.h = 100;

  SDL_RenderFillRect(renderer, &rectangle2);
}

int main(int argc, char **argv)
{
  (void)argc;
  (void)argv;

  SDL_Window *window = NULL;
  SDL_Renderer *renderer = NULL;

  int width = 800;
  int height = 800;

  int i = 0;
  int monter = 1;

  SDL_DisplayMode screen;

  if (SDL_Init(SDL_INIT_VIDEO) != 0)
    end_sdl(0, "ERROR SDL INIT", window, renderer);

  SDL_GetCurrentDisplayMode(0, &screen);
  printf("Résolution écran\n\tw : %d\n\th : %d\n", screen.w,
         screen.h);

  window = SDL_CreateWindow("Et pourquoi pas ?",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED, width,height,SDL_WINDOW_OPENGL);
  if (window == NULL)
    end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

  renderer = SDL_CreateRenderer(
      window, -1, SDL_RENDERER_ACCELERATED);
  if (renderer == NULL)
    end_sdl(0, "ERROR RENDERER CREATION", window, renderer);
  int running = 1;
  SDL_Event event;

  while (running)
  {
    while (SDL_PollEvent(&event))
    {
      switch (event.type)
      {
      case SDL_WINDOWEVENT:
        switch (event.window.event)
        {
        case SDL_WINDOWEVENT_CLOSE:
          running = 0;
          break;
        }
        break;
      case SDL_KEYDOWN:
        switch (event.key.keysym.sym)
        {
        case SDLK_ESCAPE:
          running = 0;
          break;
        case SDLK_SPACE:
          running = 0;
          break;
        case SDLK_q:
          running = 0;
          break;
        }
        break;
      case SDL_QUIT:
        running = 0;
        break;
      }
      break;
    }
    draw(renderer, i, i, 230, 68, 64);
    draw(renderer, 700 - i, 700 - i, 224, 211, 63);
    draw(renderer, i, 700 - i, 87, 156, 222);
    draw(renderer, 700 - i, i, 34, 194, 6);
    if ((monter == 1) && (i >= 300))
      monter = 0;
    if ((monter == 0) && (i <= 0))
      monter = 1;
    if ((monter) && (i < 100))
      i = i + 5 ;
    else
      if ((monter) && (i>= 100))
        i = i + 10;
      else
      {
        if ((monter) && (i >=200))
          i = i + 5;
        else
        {
          if ((monter == 0) && (i<100))
            i = i - 5;
          else
          {
            if ((monter == 0) && (i<200))
              i = i - 10;
            else          
              i = i - 5;
          }
        }
      }
    SDL_RenderPresent(renderer);
    SDL_Delay(30);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
  }

  end_sdl(1, "Normal ending", window, renderer);
  return 0;
}