#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

/*Compilation avec gcc jeu_vie.c -o jeu_vie -lSDL2 -lm -Wall -Wextra -g*/

#define N 30
#define M 50

int const survie[9] = {0, 0, 1, 1, 0, 0, 0, 0, 0};
int const naissance[9] = {0, 0, 0, 1, 0, 0, 0, 0, 0};

void initialiser_grille(int grille[N][M])
{
    srand(time(0));
    int r;
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++)
        {
            r = rand();
            if ((r % 3) == 1)
                grille[i][j] = 1;
            else
                grille[i][j] = 0;
        }
    }

    grille[2][3] = 1;
    grille[2][4] = 1;
    grille[2][5] = 1;
}

int nb_voisins_vivants(int grille[N][M], int i, int j)
{
    int nb_voisins = 0;

    nb_voisins = nb_voisins + grille[(i - 1 + N) % N][(j - 1 + M) % M];
    nb_voisins = nb_voisins + grille[(i - 1 + N) % N][(j + M) % M];
    nb_voisins = nb_voisins + grille[(i - 1 + N) % N][(j + 1 + M) % M];
    nb_voisins = nb_voisins + grille[(i + N) % N][(j - 1 + M) % M];
    nb_voisins = nb_voisins + grille[(i + N) % N][(j + 1 + M) % M];
    nb_voisins = nb_voisins + grille[(i + 1 + N) % N][(j - 1 + M) % M];
    nb_voisins = nb_voisins + grille[(i + 1 + N) % N][(j + M) % M];
    nb_voisins = nb_voisins + grille[(i + 1 + N) % N][(j + 1 + M) % M];

    return nb_voisins;
}

void recopie(int grille[N][M], int nouvgrille[N][M])
{
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++)
        {
            grille[i][j] = nouvgrille[i][j];
        }
    }
}

void survie_ou_pas(int grille[N][M])
{
    int voisins = 0;
    int nouvgrille[N][M];

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++)
        {
            voisins = nb_voisins_vivants(grille, i, j);
            if (grille[i][j])
                nouvgrille[i][j] = survie[voisins];
            else
                nouvgrille[i][j] = naissance[voisins];
        }
    }

    recopie(grille, nouvgrille);
}

void end_sdl(char ok, char const *msg, SDL_Window *window, SDL_Renderer *renderer)
{
    char msg_formated[255];
    int l;

    if (!ok)
    {
        strncpy(msg_formated, msg, 250);
        l = strlen(msg_formated);
        strcpy(msg_formated + l, " : %s\n");

        SDL_Log(msg_formated, SDL_GetError());
    }

    if (renderer != NULL)
        SDL_DestroyRenderer(renderer);
    if (window != NULL)
        SDL_DestroyWindow(window);

    SDL_Quit();

    if (!ok)
    {
        exit(EXIT_FAILURE);
    }
}

void dessin_vie(SDL_Renderer *renderer, int i, int j)
{
    SDL_Rect rectangle2;

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);

    rectangle2.x = j;
    rectangle2.y = i;
    rectangle2.w = 24;
    rectangle2.h = 30;

    SDL_RenderFillRect(renderer, &rectangle2);
}

void dessin_mort(SDL_Renderer *renderer, int i, int j)
{
    SDL_Rect rectangle2;

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

    rectangle2.x = j;
    rectangle2.y = i;
    rectangle2.w = 24;
    rectangle2.h = 30;

    SDL_RenderFillRect(renderer, &rectangle2);
}

void dessiner(int grille[N][M], SDL_Renderer *renderer)
{
    survie_ou_pas(grille);

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++)
        {
            if (grille[i][j])
                dessin_vie(renderer, i * 30, j * 24);
            else
                dessin_mort(renderer, i * 30, j * 24);
        }
    }
}

int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;

    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;

    int width = 1200;
    int height = 900;

    int grille[N][M];
    initialiser_grille(grille);

    SDL_DisplayMode screen;

    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        end_sdl(0, "ERROR SDL INIT", window, renderer);

    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw : %d\n\th : %d\n", screen.w,
           screen.h);

    window = SDL_CreateWindow("Jeu de la vie", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);
    if (window == NULL)
        end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

    renderer = SDL_CreateRenderer(
        window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL)
        end_sdl(0, "ERROR RENDERER CREATION", window, renderer);
    int running = 1;
    SDL_Event event;
    int afficher = 0;
    int vitesse_affichage = 10;

    while (running)
    {
        while ((running) && (SDL_PollEvent(&event)))
        {
            switch (event.type)
            {
            case SDL_WINDOWEVENT:
                switch (event.window.event)
                {
                case SDL_WINDOWEVENT_CLOSE:
                    running = 0;
                    break;
                }
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    running = 0;
                    break;
                case SDLK_q:
                    running = 0;
                    break;
                }
                break;
                break;
            case SDL_QUIT:
                running = 0;
                break;
            }
            break;
        }

        if (afficher >= vitesse_affichage)
        {
            afficher = 0;
            dessiner(grille, renderer);
        }

        afficher = afficher + 1;

        SDL_Delay(10);

        SDL_RenderPresent(renderer);
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    }

    end_sdl(1, "Normal ending", window, renderer);
    return 0;
}