#include <SDL2/SDL_image.h>
#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>

/*Compilation avec gcc creation_animation.c -o creation_animation -lSDL2 -lSDL2_image -lm -Wall -Wextra -g*/

void end_sdl(char ok, char const *msg, SDL_Window *window, SDL_Renderer *renderer)
{
  char msg_formated[255];
  int l;

  if (!ok)
  {
    strncpy(msg_formated, msg, 250);
    l = strlen(msg_formated);
    strcpy(msg_formated + l, " : %s\n");

    SDL_Log(msg_formated, SDL_GetError());
  }

  if (renderer != NULL)
    SDL_DestroyRenderer(renderer);
  if (window != NULL)
    SDL_DestroyWindow(window);

  SDL_Quit();

  if (!ok)
  {
    exit(EXIT_FAILURE);
  }
}

SDL_Texture *load_texture_from_image(char *file_image_name, SDL_Window *window, SDL_Renderer *renderer)
{
  SDL_Surface *my_image = NULL;   // Variable de passage
  SDL_Texture *my_texture = NULL; // La texture

  my_image = IMG_Load(file_image_name); // Chargement de l'image dans la surface
                                        // image=SDL_LoadBMP(file_image_name); fonction standard de la SDL,
                                        // uniquement possible si l'image est au format bmp */
  if (my_image == NULL)
    end_sdl(0, "Chargement de l'image impossible", window, renderer);

  my_texture = SDL_CreateTextureFromSurface(renderer, my_image); // Chargement de l'image de la surface vers la texture
  SDL_FreeSurface(my_image);                                     // la SDL_Surface ne sert que comme élément transitoire
  if (my_texture == NULL)
    end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

  return my_texture;
}

void play_with_texture_1_1(SDL_Texture *my_texture, SDL_Window *window,
                           SDL_Renderer *renderer)
{
  SDL_Rect
      source = {0},            // Rectangle définissant la zone de la texture à récupérer
      window_dimensions = {0}, // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
      destination = {0};       // Rectangle définissant où la zone_source doit être déposée dans le renderer

  SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h); // Récupération des dimensions de la fenêtre
  SDL_QueryTexture(my_texture, NULL, NULL,
                   &source.w, &source.h); // Récupération des dimensions de l'image

  destination = window_dimensions; // On fixe les dimensions de l'affichage à  celles de la fenêtre

  /* On veut afficher la texture de façon à ce que l'image occupe la totalité de la fenêtre */

  SDL_RenderCopy(renderer, my_texture,
                 &source,
                 &destination); // Création de l'élément à afficher
  // Affichage
}

void play_with_texture_5(SDL_Texture *bg_texture,
                         SDL_Texture *my_texture,
                         SDL_Texture *my_texture2,
                         SDL_Window *window,
                         SDL_Renderer *renderer)
{
  SDL_Rect
      source = {0},
      source2 = {0},            // Rectangle définissant la zone de la texture à récupérer
      window_dimensions = {0}, // Rectangle définissant la fenêtre, on  n'utilisera que largeur et hauteur
      destination = {0},
      destination2 = {0};       // Rectangle définissant où la zone_source doit être déposée dans le renderer

  SDL_GetWindowSize(window, // Récupération des dimensions de la fenêtre
                    &window_dimensions.w,
                    &window_dimensions.h);
  SDL_QueryTexture(my_texture, NULL, NULL, // Récupération des dimensions de l'image
                   &source.w, &source.h);
  SDL_QueryTexture(my_texture2, NULL, NULL, // Récupération des dimensions de l'image
                   &source2.w, &source2.h);         

  int nb_images = 64;                      //  Il y a 8 vignette dans la ligne qui nous intéresse
  int nb_images_animation = 1 * nb_images; //
  float zoom = 2;                          // zoom, car ces images sont un peu petites
  int offset_x = source.w / 8,             // La largeur d'une vignette de l'image
      offset_y = source.h / 4;             // La hauteur d'une vignette de l'image
  int offset2_x = source2.w / 8, offset2_y = source.h / 4;
  SDL_Rect state[64];                      // Tableau qui stocke les vignettes dans le bon ordre pour l'animation
  SDL_Rect state2[64];

  /* construction des différents rectangles autour de chacune des vignettes de la planche */
  int i = 0;
  for (int y = 0; y < source.h; y += offset_y)
  {
    for (int x = 0; x < source.w; x += offset_x)
    {
      state[i].x = x;
      state[i].y = y;
      state[i].w = offset_x;
      state[i].h = offset_y;
      ++i;
    }
  }

  i = 0;
  for (int y = 0; y < source2.h; y += offset2_y)
  {
    for (int x = 0; x < source2.w; x += offset2_x)
    {
      state2[i].x = x;
      state2[i].y = y;
      state2[i].w = offset2_x;
      state2[i].h = offset2_y;
      ++i;
    }
  }

  for (; i < nb_images; ++i)
  { // reprise du début de l'animation en sens inverse
    state[i] = state[63 - i];
    state2[i] = state2[63 - i];
  }

  destination.w = offset_x * zoom; // Largeur du sprite à l'écran
  destination.h = offset_y * zoom; // Hauteur du sprite à l'écran
  destination.x = 200;        // Position en x pour l'affichage du sprite
  destination.y = 300;             // Position en y pour l'affichage du sprite

  destination2.w = offset_x * zoom; // Largeur du sprite à l'écran
  destination2.h = offset_y * zoom; // Hauteur du sprite à l'écran
  destination2.x = 890;        // Position en x pour l'affichage du sprite
  destination2.y = 300;             // Position en y pour l'affichage du sprite

  i = 24;
  int k = 32;
  for (int j = 0; j < 16; j++)
  {
    destination.x += 20;
    destination2.x -= 20;

    play_with_texture_1_1(bg_texture, // identique à play_with_texture_1, où on a enlevé l'affichage et la pause
                          window, renderer);
    SDL_RenderCopy(renderer, // Préparation de l'affichage
                   my_texture, &state[i], &destination);
    i = 24 + (i + 1) % 8;
    SDL_RenderCopy(renderer, // Préparation de l'affichage
                   my_texture2, &state2[k], &destination2);
    k = 32 + (k - 1) % 8;        // Passage à l'image suivante, le modulo car l'animation est cyclique
    SDL_RenderPresent(renderer); // Affichage
    SDL_Delay(42);              // Pause en ms
  }

  i = 0;
  k = 5;

  for (int j = 0; j < 2; j++)
  {
    play_with_texture_1_1(bg_texture, // identique à play_with_texture_1, où on a enlevé l'affichage et la pause
                          window, renderer);
    SDL_RenderCopy(renderer, // Préparation de l'affichage
                   my_texture, &state[i], &destination);
    i = (i + 1) % 8;             // Passage à l'image suivante, le modulo car l'animation est cyclique
    SDL_RenderCopy(renderer, // Préparation de l'affichage
                   my_texture2, &state2[k], &destination2);
    k = (k - 1) % 8;      
    SDL_RenderPresent(renderer); // Affichage
    SDL_Delay(42);
  }

  k = 5;

  for (int j = 0; j < 2; j++)
  {
    play_with_texture_1_1(bg_texture, // identique à play_with_texture_1, où on a enlevé l'affichage et la pause
                          window, renderer);
    SDL_RenderCopy(renderer, // Préparation de l'affichage
                   my_texture, &state[i], &destination);
    i = (i + 1) % 8;             // Passage à l'image suivante, le modulo car l'animation est cyclique
    SDL_RenderCopy(renderer, // Préparation de l'affichage
                   my_texture2, &state2[k], &destination2);
    k = (k + 1) % 8;      
    SDL_RenderPresent(renderer); // Affichage
    SDL_Delay(42);
  }

  i = 8;
  k = 11;

  for (int j = 0; j < 2; j++)
  {
    play_with_texture_1_1(bg_texture, // identique à play_with_texture_1, où on a enlevé l'affichage et la pause
                          window, renderer);
    SDL_RenderCopy(renderer, // Préparation de l'affichage
                   my_texture, &state[i], &destination);
    i = i + 1;                   // Passage à l'image suivante, le modulo car l'animation est cyclique
    SDL_RenderCopy(renderer, // Préparation de l'affichage
                   my_texture2, &state2[k], &destination2);
    k = (k + 1) % 8;      
    SDL_RenderPresent(renderer); // Affichage
    SDL_Delay(42);
  }

  i = 18;
  k = 18;

  for (int j = 0; j < 2; j++)
  {
    play_with_texture_1_1(bg_texture, // identique à play_with_texture_1, où on a enlevé l'affichage et la pause
                          window, renderer);
    SDL_RenderCopy(renderer, // Préparation de l'affichage
                   my_texture, &state[i], &destination);
    i = i + 1;                   // Passage à l'image suivante, le modulo car l'animation est cyclique
    SDL_RenderCopy(renderer, // Préparation de l'affichage
                   my_texture2, &state2[k], &destination2);
    SDL_RenderPresent(renderer); // Affichage
    SDL_Delay(42);
  }

  k = 23;

  for (int j = 0; j < 2; j++)
  {
    destination2.x += 10 + 10*j;
    play_with_texture_1_1(bg_texture, // identique à play_with_texture_1, où on a enlevé l'affichage et la pause
                          window, renderer);
    SDL_RenderCopy(renderer, // Préparation de l'affichage
                   my_texture, &state[i], &destination);
    i = i + 1;                   // Passage à l'image suivante, le modulo car l'animation est cyclique
    SDL_RenderCopy(renderer, // Préparation de l'affichage
                   my_texture2, &state2[k], &destination2);
    k = k - 1;
    SDL_RenderPresent(renderer); // Affichage
    SDL_Delay(42);
  }

  SDL_RenderClear(renderer); // Effacer la fenêtre avant de rendre la main
}

int main(int argc, char **argv)
{
  (void)argc;
  (void)argv;

  SDL_Window *window = NULL;
  SDL_Renderer *renderer = NULL;

  /* Initialisation de la SDL  + gestion de l'échec possible */
  if (SDL_Init(SDL_INIT_VIDEO) != 0)
  {
    SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError()); // l'initialisation de la SDL a échoué
    exit(EXIT_FAILURE);
  }

  /* Création de la fenêtre de gauche */
  window = SDL_CreateWindow("Ensemble des affichages d'images", 0, 0, 1920, 1080, SDL_WINDOW_RESIZABLE);

  if (window == NULL)
  {
    SDL_Log("Error : SDL window 1 creation - %s\n", SDL_GetError()); // échec de la création de la fenêtre
    SDL_Quit();
    exit(EXIT_FAILURE);
  }

  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (renderer == NULL)
    end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

  SDL_Texture *bgtexture = load_texture_from_image("mur.jpg", window, renderer);
  SDL_Texture *texture1 = load_texture_from_image("greenpants.png", window, renderer);
  SDL_Texture *texture2 = load_texture_from_image("reverse_greypants.png", window, renderer);

  play_with_texture_5(bgtexture, texture1, texture2, window, renderer);

  SDL_Delay(30);

  SDL_DestroyWindow(window);
  SDL_DestroyRenderer(renderer);
  SDL_DestroyTexture(bgtexture);
  SDL_DestroyTexture(texture1);
  SDL_DestroyTexture(texture2);

  IMG_Quit();
  SDL_Quit();
  return 0;
}