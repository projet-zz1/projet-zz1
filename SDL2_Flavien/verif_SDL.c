#include <SDL2/SDL.h>
#include <stdio.h>

/*Compilation avec : gcc verif_SDL.c -o verif_SDL -lSDL2 -Wall -Wextra -g*/

int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    SDL_version nb;
    SDL_VERSION(&nb);

    printf("Version de la SDL : %d.%d.%d\n", nb.major, nb.minor, nb.patch);
    return 0;
}