#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>

/*Compilation avec : gcc x_fenetre.c -o x_fenetre -lSDL2 -lSDL2_image -Wall -Wextra -g*/

/************************************/
/*  exemple de création de fenêtres */
/************************************/

void end_sdl(char ok, char const *msg, SDL_Window *window, SDL_Renderer *renderer)
{ 
  char msg_formated[255];
  int l;

  if (!ok)
  {
    strncpy(msg_formated, msg, 250);
    l = strlen(msg_formated);
    strcpy(msg_formated + l, " : %s\n");

    SDL_Log(msg_formated, SDL_GetError());
  }

  if (renderer != NULL)
    SDL_DestroyRenderer(renderer);
  if (window != NULL)
    SDL_DestroyWindow(window);

  SDL_Quit();

  if (!ok)
  {
    exit(EXIT_FAILURE);
  }
}

SDL_Texture *load_texture_from_image(char *file_image_name, SDL_Window *window, SDL_Renderer *renderer)
{
    SDL_Surface *my_image = NULL;   // Variable de passage
    SDL_Texture *my_texture = NULL; // La texture

    my_image = IMG_Load(file_image_name); // Chargement de l'image dans la surface
                                          // image=SDL_LoadBMP(file_image_name); fonction standard de la SDL,
                                          // uniquement possible si l'image est au format bmp */
    if (my_image == NULL)
        end_sdl(0, "Chargement de l'image impossible", window, renderer);

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image); // Chargement de l'image de la surface vers la texture
    SDL_FreeSurface(my_image);                                     // la SDL_Surface ne sert que comme élément transitoire
    if (my_texture == NULL)
        end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

    return my_texture;
}

void play_with_texture_1(SDL_Texture *my_texture, SDL_Window *window,
                         SDL_Renderer *renderer) {
  SDL_Rect 
          source = {0},                         // Rectangle définissant la zone de la texture à récupérer
          window_dimensions = {0},              // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
          destination = {0};                    // Rectangle définissant où la zone_source doit être déposée dans le renderer

  SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);                    // Récupération des dimensions de la fenêtre
  SDL_QueryTexture(my_texture, NULL, NULL,
                   &source.w, &source.h);       // Récupération des dimensions de l'image

  destination = window_dimensions;              // On fixe les dimensions de l'affichage à  celles de la fenêtre

  /* On veut afficher la texture de façon à ce que l'image occupe la totalité de la fenêtre */

  SDL_RenderCopy(renderer, my_texture,
                 &source,
                 &destination);                 // Création de l'élément à afficher
  SDL_RenderPresent(renderer);                  // Affichage
  SDL_Delay(20);                              // Pause en ms

  SDL_RenderClear(renderer);                    // Effacer la fenêtre
}

SDL_Window * ouvrir_fenetre(int pos_x, int pos_y)
{
    SDL_Window * window = SDL_CreateWindow("Une fenêtre de plus", pos_x, pos_y, 1920/11, 1080/11, 0);


    SDL_Renderer * renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL)
        end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    SDL_Texture * texture = load_texture_from_image("X-men.jpg", window, renderer);

    if (window == NULL)
    {
        SDL_Log("Erreur : SDL window creation -%s\n", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    play_with_texture_1(texture, window, renderer);

    return window;
}

int main()
{
    SDL_Window * stock[22];

    if (SDL_Init(SDL_INIT_VIDEO) != 0) 
    {
        SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());      // l'initialisation de la SDL a échoué 
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < 11; i++)
    {
        stock[i] = ouvrir_fenetre(0 + i*(1920/11), 0 + i*(1080/15));
    }

    for(int i = 0; i < 11; i++)
    {
        stock[11 + i] = ouvrir_fenetre((1920 - 1920/11) - i*(1920/11), 0 + i*(1080/15));
    }

    SDL_Delay(5000);

    for (int i = 0; i < 22; i++)
    {
        SDL_DestroyWindow(stock[i]);
    }

    IMG_Quit();
    SDL_Quit();
    return 0;
}