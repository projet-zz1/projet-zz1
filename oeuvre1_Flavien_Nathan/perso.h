#ifndef _PERSO_H_
#define _PERSO_H_

#include "image.h"

void construction_image(SDL_Texture * my_texture, SDL_Window * window, SDL_Rect * destination, SDL_Rect state[], int x, int y);

void construction_image2(SDL_Texture * my_texture, SDL_Window * window, SDL_Rect * destination, SDL_Rect state[], int x, int y);

void deplacement_ballon(SDL_Rect * destination , int speed_x , int speed_y);

void dessiner_balle(SDL_Renderer * renderer , SDL_Texture * sprite , SDL_Rect state[] , SDL_Rect * destination , int i , int speed_x , int speed_y);

void deplacement_tout_droit(SDL_Texture * texture, SDL_Texture *my_texture,SDL_Window *window,SDL_Renderer *renderer, SDL_Rect * destination, SDL_Rect state[], int i);

void deplacement_droite(SDL_Texture *texture, SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer, SDL_Rect *destination, SDL_Rect state[], int i);

void deplacement_gauche(SDL_Texture *texture, SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer, SDL_Rect *destination, SDL_Rect state[], int i);

void collision(SDL_Rect * destination, SDL_Rect * destination2, int * score, int *dessiner);

#endif