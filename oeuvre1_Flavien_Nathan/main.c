#include "main.h"

int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;

    Uint8 const *keystates;

    int width = 600;
    int height = 800;

    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;
    SDL_Texture *texture = NULL;

    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        end_sdl(0, "ERROR SDL INIT", window, renderer);

    window = SDL_CreateWindow("Premier chef d'oeuvre", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);
    if (window == NULL)
        end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL)
        end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    if (TTF_Init() < 0)
        end_sdl(0, "Couldn't initialize SDL TTF", window, renderer);

    TTF_Font *font = NULL;                   // la variable 'police de caractère'
    font = TTF_OpenFont("Pacifico.ttf", 65); // La police à charger, la taille désirée
    if (font == NULL)
        end_sdl(0, "Can't load font", window, renderer);

    TTF_SetFontStyle(font, TTF_STYLE_ITALIC | TTF_STYLE_BOLD); // en italique, gras

    SDL_Color color = {255, 255, 255, 255}; // la couleur du texte
    SDL_Surface *text_surface = NULL;

    int score = 0;

    char score_aff[20] = "";

    texture = load_texture_from_image("terrain.jpg", window, renderer);

    SDL_Texture *texture2 = load_texture_from_image("football_player.png", window, renderer);
    SDL_Texture *texture3 = load_texture_from_image("ballon.png", window, renderer);

    int x = 5, y = 5, x2 = 4, y2 = 3;

    SDL_Rect destination = {0};
    SDL_Rect destination2 = {0};

    SDL_Rect state[50];
    SDL_Rect state2[50];

    construction_image(texture2, window, &destination, state, x, y);
    construction_image2(texture3, window, &destination2, state2, x2, y2);

    int running = 1;
    int droite = 0;
    int gauche = 0;
    int dessiner = 1;

    int i = 0;

    while (running)
    {
        SDL_Event event;
        while ((running) && (SDL_PollEvent(&event)))
        {
            switch (event.type)
            {
            case SDL_WINDOWEVENT:
                switch (event.window.event)
                {
                case SDL_WINDOWEVENT_CLOSE:
                    running = 0;
                    break;
                }
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    running = 0;
                    break;
                case SDLK_SPACE:
                    running = 0;
                    break;
                }
                break;
            case SDL_QUIT:
                running = 0;
                break;
            }
            break;
        }
        keystates = SDL_GetKeyboardState(NULL);
        if (keystates[SDL_SCANCODE_ESCAPE])
            running = 0;
        if (keystates[SDL_SCANCODE_SPACE])
            running = 0;
        if (keystates[SDL_SCANCODE_Q])
            running = 0;
        if (keystates[SDL_SCANCODE_RIGHT])
            droite = 1;
        if (keystates[SDL_SCANCODE_LEFT])
            gauche = 1;

        if (droite)
        {
            deplacement_droite(texture, texture2, window, renderer, &destination, state, i);
            droite = 0;
        }
        else
        {
            if (gauche)
            {
                deplacement_gauche(texture, texture2, window, renderer, &destination, state, i);
                gauche = 0;
            }
            else
                deplacement_tout_droit(texture, texture2, window, renderer, &destination, state, i);
        }

        if (dessiner)
            dessiner_balle(renderer, texture3, state2, &destination2, i, 0, 10);

        collision(&destination2, &destination, &score, &dessiner);

        afficher_texte(font, score_aff, score, color, text_surface, window, renderer);

        SDL_RenderPresent(renderer);

        i++;

        SDL_Delay(40);
    }

    SDL_DestroyTexture(texture);
    SDL_DestroyTexture(texture2);

    end_sdl(1, "Normal ending", window, renderer);
    return 0;
}