#include "texte.h"

void afficher_texte(TTF_Font *font, char score_aff[20], int score, SDL_Color color, SDL_Surface *text_surface, SDL_Window *window, SDL_Renderer *renderer)
{
    sprintf(score_aff, "Score : %d", score);

    text_surface = TTF_RenderText_Blended(font, score_aff, color); // création du texte dans la surface
    if (text_surface == NULL)
        end_sdl(0, "Can't create text surface", window, renderer);

    SDL_Texture *text_texture = NULL;                                    // la texture qui contient le texte
    text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
    if (text_texture == NULL)
        end_sdl(0, "Can't create texture from surface", window, renderer);
    SDL_FreeSurface(text_surface);

    SDL_Rect pos = {0, 0, 0, 0};                                // rectangle où le texte va être prositionné
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h); // récupération de la taille (w, h) du texte
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);

    SDL_DestroyTexture(text_texture);
}