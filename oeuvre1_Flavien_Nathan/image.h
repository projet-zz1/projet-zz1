#ifndef _IMAGE_H_
#define _IMAGE_H_

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

void end_sdl(char ok, char const *msg, SDL_Window *window, SDL_Renderer *renderer);

SDL_Texture *load_texture_from_image(char *file_image_name, SDL_Window *window, SDL_Renderer *renderer);

void deplacement_fond(SDL_Texture *my_texture,SDL_Window *window,SDL_Renderer *renderer, int i, int * animation);

#endif