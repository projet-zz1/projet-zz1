#include "perso.h"

void construction_image(SDL_Texture *my_texture, SDL_Window *window, SDL_Rect *destination, SDL_Rect state[], int x, int y)
{
    SDL_Rect source = {0}, window_dimensions = {0};

    SDL_GetWindowSize(window, // Récupération des dimensions de la fenêtre
                      &window_dimensions.w,
                      &window_dimensions.h);
    SDL_QueryTexture(my_texture, NULL, NULL, // Récupération des dimensions de l'image
                     &source.w, &source.h);
          //  Il y a 8 vignette dans la ligne qui nous intéresse
    float zoom = 1;              // zoom, car ces images sont un peu petites
    int offset_x = source.w / x, // La largeur d'une vignette de l'image
        offset_y = source.h / y; // La hauteur d'une vignette de l'image

    int i = 0;
    for (int y = 0; y < source.h; y += offset_y)
    {
        for (int x = 0; x < source.w; x += offset_x)
        {
            state[i].x = x;
            state[i].y = y;
            state[i].w = offset_x;
            state[i].h = offset_y;
            ++i;
        }
    }

    destination->w = offset_x * zoom;                              // Largeur du sprite à l'écran
    destination->h = offset_y * zoom;                              // Hauteur du sprite à l'écran
    destination->x = window_dimensions.w / 2 - destination->w / 2; // Position en x pour l'affichage du sprite
    destination->y = window_dimensions.h - destination->h -30;         // Position en y pour l'affichage du sprite
}

void construction_image2(SDL_Texture *my_texture, SDL_Window *window, SDL_Rect *destination, SDL_Rect state[], int x, int y)
{
    SDL_Rect source = {0}, window_dimensions = {0};

    SDL_GetWindowSize(window, // Récupération des dimensions de la fenêtre
                      &window_dimensions.w,
                      &window_dimensions.h);
    SDL_QueryTexture(my_texture, NULL, NULL, // Récupération des dimensions de l'image
                     &source.w, &source.h);
          //  Il y a 8 vignette dans la ligne qui nous intéresse
    float zoom = 1;              // zoom, car ces images sont un peu petites
    int offset_x = source.w / x, // La largeur d'une vignette de l'image
        offset_y = source.h / y; // La hauteur d'une vignette de l'image

    int i = 0;
    for (int y = 0; y < source.h; y += offset_y)
    {
        for (int x = 0; x < source.w; x += offset_x)
        {
            state[i].x = x;
            state[i].y = y;
            state[i].w = offset_x;
            state[i].h = offset_y;
            ++i;
        }
    }

    destination->w = offset_x * zoom;                              // Largeur du sprite à l'écran
    destination->h = offset_y * zoom;;                              // Hauteur du sprite à l'écran
    destination->x = 50; // Position en x pour l'affichage du sprite
    destination->y = 90;         // Position en y pour l'affichage du sprite
}

void deplacement_ballon(SDL_Rect * destination , int speed_x , int speed_y) //fonction qui actualise le rectangle destination
{                                                                                               //ainsi que le sprite a utiliser
    destination->x = destination->x + speed_x;
    destination->y = destination->y + speed_y;
}

void dessiner_balle(SDL_Renderer * renderer , SDL_Texture * sprite , SDL_Rect state[] , SDL_Rect * destination , int i , int speed_x , int speed_y)
{                                                                                               //fonction qui affiche le sprite a l'image puis l'actualise
    SDL_RenderCopy(renderer , sprite , &state[i%12] , destination);                               //i correspod au "numero" de l'image, i au nombre total de sprites (taille de state)
    deplacement_ballon(destination ,speed_x , speed_y);
}

void deplacement_tout_droit(SDL_Texture *texture, SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer, SDL_Rect *destination, SDL_Rect state[], int i)
{
    int animation = 1;
    deplacement_fond(texture, window, renderer, i, &animation);
    if (animation)
        SDL_RenderCopy(renderer, my_texture, &state[1 + 5 * i % 25], destination); // Passage à l'image suivante, le modulo car l'animation est cyclique
}

void deplacement_droite(SDL_Texture *texture, SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer, SDL_Rect *destination, SDL_Rect state[], int i)
{
    SDL_Rect window_dimensions = {0};
    SDL_GetWindowSize(window,
                      &window_dimensions.w,
                      &window_dimensions.h);
    int animation = 1;
    deplacement_fond(texture, window, renderer, i, &animation);
    if ((animation) && (destination->x < window_dimensions.w - 65))
    {
        destination->x = destination->x + 6;
        SDL_RenderCopy(renderer, my_texture, &state[17 + i % 3], destination);
    }
    else
        SDL_RenderCopy(renderer, my_texture, &state[17 + i % 3], destination);
}

void deplacement_gauche(SDL_Texture *texture, SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer, SDL_Rect *destination, SDL_Rect state[], int i)
{
    int animation = 1;
    deplacement_fond(texture, window, renderer, i, &animation);
    if ((animation) && (destination->x > -5))
    {
        destination->x = destination->x - 6;
        SDL_RenderCopy(renderer, my_texture, &state[22 + i % 3], destination);
    }
    else
        SDL_RenderCopy(renderer, my_texture, &state[22 + i % 3], destination);
}

void collision(SDL_Rect * destination, SDL_Rect * destination2, int * score, int * dessiner)
{
    if ((*dessiner) && (destination->y - destination->h + 90>= destination2->y) && (destination->y - destination->h + 80 <= destination2->y) && (((destination->x + destination->w >= destination2->x) && (destination->x <= destination2->x)) || ((destination->x <= destination2->x + destination2->w) && (destination->x + destination->w >= destination2->x + destination2->w))))
    {
        *score = *score + 5;
        *dessiner = 0;
    }
}