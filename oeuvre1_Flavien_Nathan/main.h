#ifndef _MAIN_H_
#define _MAIN_H_

#include "image.h"
#include "perso.h"
#include "texte.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

int main(int argc, char ** argv);

#endif