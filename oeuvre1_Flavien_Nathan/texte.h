#ifndef _TEXTE_H_
#define _TEXTE_H_

#include "perso.h"

void afficher_texte(TTF_Font *font, char score_aff[20], int score, SDL_Color color, SDL_Surface *text_surface, SDL_Window *window, SDL_Renderer *renderer);

#endif