#ifndef PARTITIONS_H
#define PARTITIONS_H

typedef struct partition_arb {
    int * partition;
    int * hauteurs;
    int nb_el;
} partition_arb_t;

int AllouerPartition(partition_arb_t * part_arb , int n);

void CreerPartition(partition_arb_t * partarb , int n);

int RecupererClasse(partition_arb_t * part_arb , int a);

#endif