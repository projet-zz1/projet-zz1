#include <stdio.h>
#include <stdlib.h>
#include "CodePartitions.h"

int AllouerPartition(partition_arb_t * part_arb , int n)
{
    int erreur = 0;

    part_arb = (partition_arb_t *) malloc(sizeof(partition_arb_t));
    if (part_arb)
    {
        part_arb->partition = (int *) malloc(sizeof(int) * (n+1));
        if (part_arb->partition)
        {
            part_arb->hauteurs = (int *) malloc(sizeof(int) * (n+1));
            if (!part_arb->hauteurs)
            {
                erreur = 1;
            }
        }
        else
        {
            erreur = 1;
        }
    }
    else
    {
        erreur = 1;
    }

    return(erreur);
}

void CreerPartition(partition_arb_t * part_arb , int n)
{
    int erreur;
    int i;

    erreur = AllouerPartition(part_arb , n);
    if (!erreur)
    {
        for (i=0 ; i<=n ; i++)
        {
            *(part_arb->partition + i) = i;
            *(part_arb->hauteurs + i) = 1;
        }
        part_arb->nb_el = n+1;
    }
    else
    {
        printf("Erreur a la creation de la partition\n");
    }
}

int RecupererClasse(partition_arb_t * part_arb , int a)
{
    int i;
    int h = *(part_arb->hauteurs + a);
    int classe = *(part_arb->partition + a);

    for (i=0 ; i<h ; i++)
    {
        classe = *(part_arb->partition + classe);
    }

    return(classe);
}