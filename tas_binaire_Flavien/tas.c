#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 1000
//gcc tas.c -o tas -Wall -Wextra -g

typedef struct tas_binaires
{
    int *data;
    int fin;
    int size;
} tas_bin;

void liberer_tas(tas_bin *tas)
{
    free(tas->data);
    free(tas);
}

tas_bin *creer(int n)
{
    tas_bin *tas = malloc(sizeof(tas_bin));
    if (tas)
    {
        tas->data = malloc((n + 1) * sizeof(int));
        if (tas->data)
        {
            tas->fin = 0;
            tas->size = n;
        }
    }
    return tas;
}

int min(int a, int b)
{
    if (a > b)
        return b;
    else
        return a;
}

void inverser(int *data, int i, int j)
{
    int temp = 0;
    temp = data[i];
    data[i] = data[j];
    data[j] = temp;
}

void ajouter(tas_bin *tas, int x)
{
    if (tas->fin < tas->size)
    {
        tas->data[tas->fin + 1] = x;
        tas->fin = tas->fin + 1;
        int cour = tas->fin;

        while (cour > 1 && tas->data[cour / 2] > tas->data[cour])
        {
            inverser(tas->data, cour, cour / 2);
            cour = cour / 2;
        }
    }
}

int defiler(tas_bin *tas)
{
    if (tas->fin > 0)
    {
        int stock = tas->data[1];
        tas->data[1] = tas->data[tas->fin];
        tas->fin = tas->fin - 1;
        int cour = 1;
        while ((2 * cour + 1) <= tas->fin && (tas->data[cour] > min(tas->data[2 * cour], tas->data[2 * cour + 1])))
        {
            if (tas->data[2 * cour + 1] < tas->data[2 * cour])
            {
                inverser(tas->data, cour, 2 * cour + 1);
                cour = 2 * cour + 1;
            }
            else
            {
                inverser(tas->data, cour, 2 * cour);
                cour = 2 * cour;
            }
        }
        if ((2 * cour) == tas->fin && tas->data[cour] > tas->data[tas->fin])
        {
            inverser(tas->data, cour, tas->fin);
        }
        return stock;
    }
    return -1;
}

void afficher(tas_bin *tas)
{
    for (int k = 1; k <= tas->fin; k++)
    {
        printf("%d\n", tas->data[k]);
    }
}

int intComparator(const void *first, const void *second)
{
    int firstInt = *(const int *)first;
    int secondInt = *(const int *)second;
    return firstInt - secondInt;
}

void genereralea(int tableau[N])
{
    for (int k = 0; k < N; k++)
    {
        tableau[k] = rand() % 128;
    }
}

void tri_tas(int tableau[N])
{
    tas_bin *tas = creer(N);
    for (int k = 0; k < N; k++)
    {
        ajouter(tas, tableau[k]);
    }
    for (int k = 0; k < N; k++)
    {
        tableau[k] = defiler(tas);
    }
    liberer_tas(tas);
}

int main()
{
    tas_bin *tas = creer(10);
    ajouter(tas, 5);
    ajouter(tas, 2);
    ajouter(tas, 8);
    ajouter(tas, 7);
    ajouter(tas, 1);
    afficher(tas);
    int i = defiler(tas);
    afficher(tas);
    printf("%d\n", i); //jeu de test
    srand(time(0));
    int tableau[N], tableau2[N];
    genereralea(tableau);
    for (int k = 0; k < N; k++)
    {
        tableau2[k] = tableau[k];
    }
    clock_t begin = clock();
    qsort(tableau, N, sizeof(int), intComparator);
    clock_t end = clock();
    printf("temps pour le qsort : %ld\n", end - begin);
    begin = clock();
    tri_tas(tableau2);
    end = clock();
    printf("temps pour le tas binaire : %ld\n", end - begin);
    liberer_tas(tas);
}