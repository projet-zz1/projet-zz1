#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include<time.h>

#define PI 3.14

void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
                  char const* msg,                                    // message à afficher
                  SDL_Window* window,                                 // fenêtre à fermer
                  SDL_Renderer* renderer) {                           // renderer à fermer
  char msg_formated[255];                                         
  int l;                                                          

  if (!ok) {                                                      
         strncpy(msg_formated, msg, 250);                                 
         l = strlen(msg_formated);                                        
         strcpy(msg_formated + l, " : %s\n");                     

         SDL_Log(msg_formated, SDL_GetError());                   
  }                                                               

  if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
  if (window != NULL)   SDL_DestroyWindow(window);                                        

  SDL_Quit();                                                     

  if (!ok) {                                                      
         exit(EXIT_FAILURE);                                              
  }                                                               
}

void DrawRectIncline(SDL_Renderer * renderer , SDL_Rect rectangle , float angle , int couleur[3] , int delai)
{
    int x1 , y1 , x2 , y2 , x3 , y3 , x4 , y4;
    int x_pivot , y_pivot;
    int erreur;
    
    x_pivot = rectangle.x + rectangle.w / 2;
    y_pivot = rectangle.y + rectangle.h / 2;

    x1 = x_pivot - rectangle.w * cos(angle) / 2;
    y1 = y_pivot - rectangle.w * sin(angle) / 2; 
    x2 = x_pivot + rectangle.w * cos(angle) / 2;
    y2 = y_pivot + rectangle.w * sin(angle) / 2;
    x3 = x2 - rectangle.h * sin(angle);
    y3 = y2 + rectangle.h * cos(angle);
    x4 = x1 - rectangle.h * sin(angle);
    y4 = y1 + rectangle.h * cos(angle);

    SDL_SetRenderDrawColor(renderer,couleur[0], couleur[1], couleur[2],255);                                  
    SDL_RenderDrawLine(renderer , x1 , y1 , x2 , y2);
    SDL_RenderDrawLine(renderer , x2 , y2 , x3  , y3);
    SDL_RenderDrawLine(renderer , x3 , y3 , x4  , y4);
    SDL_RenderDrawLine(renderer , x1 , y1 , x4  , y4);

    SDL_RenderPresent(renderer);
    SDL_SetRenderDrawColor(renderer,0, 0, 0,255);
    erreur = SDL_RenderClear(renderer);
    SDL_Delay(delai);
}

int main()
{
    SDL_Renderer * renderer = NULL;
    SDL_Window * window = NULL;
    SDL_Rect rectangle1 , rectangle2;
    int couleur[3];
    int i;
    int delai;
    int germe = time(0);

    srand(germe);

    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    window = SDL_CreateWindow("fenetre test" , 0 , 0 , 1000 , 1000 , 0);
    if (window == NULL)
    {
        SDL_Log("Error : SDL window 2 creation - %s\n", SDL_GetError());
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    couleur[0] = 255;
    couleur[1] = 0;
    couleur[2] = 0;
    rectangle1.x = 500;
    rectangle1.y = 500;
    rectangle1.w = 48;
    rectangle1.h = 256;

    for (i=0 ; i<360 ; i++)
    {
        DrawRectIncline(renderer,rectangle1, 3 * i * PI / 180 ,couleur , (i%90)/3+10);
    }

    end_sdl(1, "Normal ending", window, renderer);

    return(0);
}