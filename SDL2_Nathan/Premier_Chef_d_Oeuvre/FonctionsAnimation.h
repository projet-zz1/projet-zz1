#ifndef FONCTIONS_ANIMATIONS_H
#define FONCTIONS_ANIMATIONS_H


void ApplyBackground(SDL_Renderer * renderer , SDL_Texture * background , SDL_Rect * source , SDL_Rect * destination);

void Movement(SDL_Rect window_dimensions , SDL_Rect * destination , int y0 , int i , int nb_it);

void ApplyMovingBackground(SDL_Renderer * renderer , SDL_Texture * texture , SDL_Rect window_dimensions , SDL_Rect * source , SDL_Rect * destination , int y0 , int i , int nb_it , int fading);

void RuningSprite(SDL_Rect * destination , SDL_Rect window_dimensions , int * i , int nb_sprite);

void ApplySprite(SDL_Renderer * renderer , SDL_Texture * sprite ,SDL_Rect state[] , SDL_Rect * destination , SDL_Rect window_dimensions , int * i , int nb_sprite_x);

void MovementBall(SDL_Rect * destination , int * i , int nb_sprite , int speed_x , int speed_y);

#endif