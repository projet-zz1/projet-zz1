#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "main.h"
#include "AffichageAnimation.h"

#define NB_BACKGROUND 1

int main(int argc , char * argv[])
{
    SDL_Window * window = NULL;
    SDL_Renderer * renderer = NULL;
    SDL_Texture * Texture[argc-1];
    int Coord[argc-2][2];
    float Zoom[argc-2];
    int Nb_sprite_state[1][2];
    int i;

    if (SDL_Init(SDL_INIT_VIDEO) != 0) 
    {
        end_sdl(0, "ERROR SDL INIT", window, renderer);
    }

    window = SDL_CreateWindow("Animation" , 0 , 0 , 1000 , 600 , 0);
    if (window == NULL)
    {
        end_sdl(0, "ERROR WINDOW CREATION", window, renderer);
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL)
    {
        end_sdl(0, "ERROR RENDERER CREATION", window, renderer);
    }

    for (i=0 ; i<argc-(1+NB_BACKGROUND) ; i++)
    {
        Texture[i] = ImageToTexture(argv[i+1] , window , renderer);
    }
    for (i=NB_BACKGROUND ; i<argc-1 ; i++)
    {
        Texture[i] = ImageToTexture(argv[i+1] , window , renderer);
    }
    Nb_sprite_state[0][0] = 8;
    Nb_sprite_state[0][1] = 4;

    Coord[0][0] = 0;
    Coord[0][1] = 0;
    Coord[1][0] = 0;
    Coord[1][1] = 0;
    Zoom[0] = 1;
    Zoom[1] = 2;

    SpriteWithBackground(Texture , window , renderer , Coord , Zoom , Nb_sprite_state);

    for (i=0 ; i<argc-1 ; i++)
        SDL_DestroyTexture(Texture[i]);

    end_sdl(1, "Normal ending", window, renderer);

    return 0;
}