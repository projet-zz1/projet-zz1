#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <math.h>
#include "FonctionsAnimation.h"

#define PI 3.14


void ApplyBackground(SDL_Renderer * renderer , SDL_Texture * background , SDL_Rect * source , SDL_Rect * destination)
{
    SDL_RenderCopy(renderer , background , source , destination);
}

void Movement(SDL_Rect window_dimensions , SDL_Rect * destination , int y0 , int i , int nb_it)
{
    float h = window_dimensions.h - destination->h - y0;

    destination->x += 2;
    destination->y = y0 + h * (1 - exp(-5.0 * i / nb_it) / 2 * (1 + cos(10.0 * i / nb_it * 2 * PI)));
}

void ApplyMovingBackground(SDL_Renderer * renderer , SDL_Texture * texture , SDL_Rect window_dimensions , SDL_Rect * source , SDL_Rect * destination , int y0 , int i , int nb_it , int fading)
{
    Movement(window_dimensions , destination , y0 , i , nb_it);
    if (fading)
    {
        SDL_SetTextureAlphaMod(texture,(1.0-1.0*i/nb_it)*255);
    }

    SDL_RenderCopy(renderer , texture , source , destination);
}

void RuningSprite(SDL_Rect * destination , SDL_Rect window_dimensions , int * i , int nb_sprite)
{
    int speed = 12;

    (*destination).x = ((*destination).x + speed) % (window_dimensions.w - (*destination).w);
    *i = 24 + (*i + 1) % nb_sprite;
    (*destination).y += 2;
}

void ApplySprite(SDL_Renderer * renderer , SDL_Texture * sprite ,SDL_Rect state[] , SDL_Rect * destination , SDL_Rect window_dimensions , int * i , int nb_sprite_x)
{
    SDL_RenderCopy(renderer , sprite , &state[*i] , destination);
    RuningSprite(destination , window_dimensions , i , nb_sprite_x);
}


void MovementBall(SDL_Rect * destination , int * i , int nb_sprite , int speed_x , int speed_y) //fonction qui actualise le rectangle destination
{                                                                                               //ainsi que le sprite a utiliser
    (*destination).x = (*destination).x + speed_x;
    (*destination).y = (*destination).y + speed_y;
    *i = (*i + 1) % nb_sprite;
}

void DrawBall(SDL_Renderer * renderer , SDL_Texture * sprite , SDL_Rect state[] , SDL_Rect * destination , int * i , int nb_sprite , int speed_x , int speed_y)
{                                                                                               //fonction qui affiche le sprite a l'image puis l'actualise
    SDL_RenderCopy(renderer , sprite , &state[*i] , destination);                               //i correspod au "numero" de l'image, i au nombre total de sprites (taille de state)
    MovementBall(destination , i , nb_sprite , speed_x , speed_y);
}