#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "AffichageAnimation.h"
#include "FonctionsAnimation.h"

#define MAX_SPRITE 40

void end_sdl(char ok , char const* msg , SDL_Window* window , SDL_Renderer* renderer)
{
    char msg_formated[255];                                         
    int l;                                                          

    if (!ok)
    {                                                      
        strncpy(msg_formated, msg, 250);                                 
        l = strlen(msg_formated);                                        
        strcpy(msg_formated + l, " : %s\n");                     
        SDL_Log(msg_formated, SDL_GetError());                   
    }                                                               

    if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
    if (window != NULL)   SDL_DestroyWindow(window);                                        

    SDL_Quit();                                                     

    if (!ok)
    {
        exit(EXIT_FAILURE);                                              
    }                                                               
}

SDL_Texture * ImageToTexture(char * file_name , SDL_Window * window , SDL_Renderer * renderer)
{
    SDL_Surface * image = NULL;
    SDL_Texture * texture = NULL;

    image = IMG_Load(file_name);
    if (image == NULL)
    {
        end_sdl(0 , "chargement de l'image impossible" , window , renderer);
    }

    texture = SDL_CreateTextureFromSurface(renderer , image);
    if (texture == NULL)
    {
        end_sdl(0 , "chargement de l'image impossible" , window , renderer);
    }

    SDL_FreeSurface(image);

    return(texture);
}

void InitBackground(SDL_Texture * background , SDL_Rect * source , SDL_Rect * destination_background , SDL_Rect window_dimensions)
{
    SDL_QueryTexture(background , NULL , NULL , &source->w , &source->h);

    *destination_background = window_dimensions;
}

void InitObjectBackground(SDL_Texture * background , SDL_Rect * source , SDL_Rect * destination , int x0 , int y0 , float zoom)
{    
    SDL_QueryTexture(background , NULL , NULL , &source->w , &source->h);

    destination->w = source->w * zoom;
    destination->h = source->h * zoom;
    destination->x = x0;
    destination->y = y0;
}

void InitSprite(SDL_Texture * sprite , SDL_Rect state[] , SDL_Rect * destination_sprite , int nb_sprite_x , int nb_sprite_y , int x0 , int y0 , float zoom)
{
    int offset_x , offset_y;
    int x , y;
    int i = 0;
    SDL_Rect source = {0};
    
    SDL_QueryTexture(sprite , NULL , NULL , &source.w , &source.h);
    offset_x = source.w / nb_sprite_x;
    offset_y = source.h / nb_sprite_y;

    for (y=0 ; y<source.h ; y+=offset_y)
    {
        for (x=0 ; x<source.w ; x+=offset_x)
        {
            state[i].x = x;
            state[i].y = y;
            state[i].w = offset_x;
            state[i].h = offset_y;
            ++i;
        }
    }

    destination_sprite->w = offset_x * zoom;
    destination_sprite->h = offset_y * zoom;
    destination_sprite->x = x0;
    destination_sprite->y = y0;
}

void SpriteWithBackground(SDL_Texture * Texture[] , SDL_Window * window , SDL_Renderer * renderer , int Coord[][2] , float Zoom[] , int Nb_sprite_state[][2])
{
    SDL_Rect window_dimensions = {0},
             destination_background = {0},
             destination_object = {0},
             destination_sprite = {0},
             source_init = {0};
    SDL_Rect state[MAX_SPRITE];
    SDL_Rect Source[3];
    int i;
    int compt;

    for (i=0 ; i<3 ; i++)
    {
        Source[i] = source_init;
    }


    SDL_GetWindowSize(window , &window_dimensions.w , &window_dimensions.h);

    //Initialisation des differents elements propres aux image/sprites
    InitBackground(Texture[0] , &Source[0] , &destination_background , window_dimensions);
    InitObjectBackground(Texture[1] , &Source[1] , &destination_object , Coord[0][0] , Coord[0][1] , Zoom[0]);
    InitSprite(Texture[2] , state , &destination_sprite , Nb_sprite_state[0][0] , Nb_sprite_state[0][1] , Coord[1][0] , Coord[1][1] , Zoom[1]);



    i = 24;
    for(compt=0 ; compt<200 ; compt++)
    {
        SDL_RenderClear(renderer);

        //fonctions d'animation a utiliser
        ApplyBackground(renderer , Texture[0] , &Source[0] , &destination_background);
        ApplyMovingBackground(renderer , Texture[1] , &Source[1] , &destination_object , window_dimensions , Coord[0][1] , compt , 200 , 1);
        ApplySprite(renderer , Texture[2] , state , &destination_sprite , window_dimensions , &i , Nb_sprite_state[0][0]);

        SDL_RenderPresent(renderer);
        SDL_Delay(50);
    }
    

    SDL_RenderClear(renderer);
}