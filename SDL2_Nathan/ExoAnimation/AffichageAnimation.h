#ifndef AFFICHAGE_ANIMATION_H
#define AFFICHAGE_ANIMATION_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

void end_sdl(char ok , char const* msg , SDL_Window* window , SDL_Renderer* renderer);

SDL_Texture * ImageToTexture(char * file_name , SDL_Window * window , SDL_Renderer * renderer);

void InitBackground(SDL_Texture * background , SDL_Rect * source , SDL_Rect * destination_background , SDL_Rect window_dimensions);

void InitObjectBackground(SDL_Texture * background , SDL_Rect * source , SDL_Rect * destination , int x0 , int y0 , float zoom);

void InitSprite(SDL_Texture * sprite , SDL_Rect state[] , SDL_Rect * destination_sprite , int nb_sprite_x , int nb_sprite_y , int x0 , int y0 , float zoom);

void SpriteWithBackground(SDL_Texture * Texture[] , SDL_Window * window , SDL_Renderer * renderer , int Coord[][2] , float Zoom[] , int Nb_sprite_state[][2]);

#endif