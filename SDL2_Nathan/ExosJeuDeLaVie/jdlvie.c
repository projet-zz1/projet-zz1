#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include<time.h>

#define K 100
#define M 30
#define N 50


void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
                    char const* msg,                                    // message à afficher
                    SDL_Window* window,                                 // fenêtre à fermer
                    SDL_Renderer* renderer) {                           // renderer à fermer
    char msg_formated[255];                                         
    int l;                                                          

    if (!ok)
    {                                                      
        strncpy(msg_formated, msg, 250);                                 
        l = strlen(msg_formated);                                        
        strcpy(msg_formated + l, " : %s\n");                     
        SDL_Log(msg_formated, SDL_GetError());                   
    }                                                               

    if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
    if (window != NULL)   SDL_DestroyWindow(window);                                        

    SDL_Quit();                                                     

    if (!ok)
    {
        exit(EXIT_FAILURE);                                              
    }                                                               
}


void Init(int grille[M][N])
{
    int i,j;
    int repartition;

    for (i=0 ; i<M ; i++)
    {
        for (j=0 ; j<N ; j++)
        {
            repartition = rand() % 5;
            if (repartition >=3)
            {
                grille[i][j] = 1;
            }
            else
            {
                grille[i][j] = 0;
            }
        }
    }
}


int NbVoisins(int grille[M][N] , int k , int l)
{
    int i,j;
    int compt = 0;

    for (i=-1 ; i<2 ; i++)
    {
        for (j=-1 ; j<2 ; j++)
        {
            compt += grille[(k+i)%M][(l+j)%N];
        }
    }
    compt = compt - grille[k][l];

    return(compt);
}


void CopieTab(int temp[M][N] , int grille[M][N])
{
    int i,j;

    for (i=0 ; i<M ; i++)
    {
        for (j=0 ; j<N ; j++)
        {
            grille[i][j] = temp[i][j];
        }
    }
}


void Actualisation(int grille[M][N])
{
    int i,j;
    int temp[M][N];
    int compt;

    for (i=0 ; i<M ; i++)
    {
        for (j=0 ; j<N ; j++)
        {
            compt = NbVoisins(grille,i,j);
            if (grille[i][j] == 1)
            {
                if ((compt < 2) || (compt > 3))
                {
                    temp[i][j] = 0;
                }
                else
                {
                    temp[i][j] = 1;
                }
            }
            else
            {
                if (compt == 3)
                {
                    temp[i][j] = 1;
                }
                else
                {
                    temp[i][j] = 0;
                }
            }
        }
    }
    CopieTab(temp , grille);
}


void AffichageGrille(int grille[M][N] , SDL_Renderer * renderer , SDL_Rect rect , SDL_DisplayMode mode)
{
    int i,j;

    rect.h = mode.h / M;
    rect.w = mode.w / N;
    
    for (i=0 ; i<M ; i++)
    {
        rect.y = i * rect.h;
        for (j=0 ; j<N ; j++)
        {
            rect.x = j * rect.w;

            if (grille[i][j])
            {
                SDL_SetRenderDrawColor(renderer , 255 , 255 , 255 , 255);
            }
            else
            {
                SDL_SetRenderDrawColor(renderer , 0 , 0 , 0 , 255);
            }
            SDL_RenderFillRect(renderer, &rect);
        }
    }
    SDL_RenderPresent(renderer);
    SDL_Delay(250);
}

int main()
{
    SDL_Window * window = NULL;
    SDL_Renderer * renderer = NULL;
    SDL_Rect rect;
    SDL_DisplayMode mode;
    int erreur;
    int k;
    int grille[M][N];
    int germe = time(0);
    
    srand(germe);

    if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);
    erreur = SDL_GetCurrentDisplayMode(0 , &mode);

    window = SDL_CreateWindow("Jeu de la vie" , 0 , 0 , mode.w , mode.h , 0);
    if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

    renderer = SDL_CreateRenderer(
            window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    Init(grille);
    AffichageGrille(grille , renderer , rect , mode);
    for (k=0 ; k<K ; k++)
    {
        Actualisation(grille);
        AffichageGrille(grille , renderer , rect , mode);
    }

    end_sdl(1, "Normal ending", window, renderer);

    return 0;
}