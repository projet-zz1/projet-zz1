#include <SDL2/SDL.h>
#include <stdio.h>


SDL_Window * CreerMultFenetre(int x , int y)
{
    SDL_Window * window;

    window = SDL_CreateWindow("fenetre test" , x , y , 400 , 300 , 0);

    if (window == NULL)
    {
        SDL_Log("Error : SDL window 2 creation - %s\n", SDL_GetError());
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    return(window);
}

int main()
{
    SDL_DisplayMode current;
    int centre_x , centre_y;
    int i;
    int n = 15;
    SDL_Window * T[4 * n + 1];


    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    

    SDL_GetCurrentDisplayMode(0 , &current);
    centre_x = current.w / 2;
    centre_y = current.h / 2;
    T[0] = CreerMultFenetre(centre_x - 200 , centre_y - 150);
    
    for (i=1 ; i<4*n+1 ; i+=4)
    {
        T[i] = CreerMultFenetre(centre_x - (200 + (i/4) * centre_x / n) , centre_y - (150 + (i/4) * centre_y / n));
        T[i+1] = CreerMultFenetre(centre_x - (200 + (i/4) * centre_x / n) , centre_y - (150 - (i/4) * centre_y / n));
        T[i+2] = CreerMultFenetre(centre_x - (200 - (i/4) * centre_x / n) , centre_y - (150 - (i/4) * centre_y / n));
        T[i+3] = CreerMultFenetre(centre_x - (200 - (i/4) * centre_x / n) , centre_y - (150 + (i/4) * centre_y / n));
    }

    SDL_Delay(10000);

    for (i=0 ; i<4*n+1 ; i++)
    {
        SDL_DestroyWindow(T[i]);
    }
    SDL_Quit();

    return 0;
}