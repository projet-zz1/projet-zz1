#include <stdio.h>
#include <stdlib.h>

//gcc partition.c -o partition -Wall -Wextra -g

typedef struct cellule_t
{
    int num;
    struct cellule_t *suiv;
} cellule;

typedef struct intermediaire
{
    int taille;
    cellule **data;
} tableauliste;

typedef struct classe_t
{
    int taille;
    int *pere;
    int *hauteur;
} classe;

classe *creer(int n)
{
    classe *partition = malloc(sizeof(classe));
    partition->taille = n;
    partition->pere = malloc(n * sizeof(int));
    partition->hauteur = malloc(n * sizeof(int));
    for (int k = 0; k < n; k++)
    {
        partition->pere[k] = k;
        partition->hauteur[k] = 1;
    }
    return partition;
}

int recuperer_classe(classe *partition, int x)
{
    while (partition->pere[x] != x)
    {
        x = partition->pere[x];
    }
    return x;
}

void fusion(classe *partition, int x, int y)
{
    int clx = recuperer_classe(partition, x);
    int cly = recuperer_classe(partition, y);
    if (partition->hauteur[clx] < partition->hauteur[cly])
    {
        partition->pere[clx] = cly;
    }
    else
    {
        if (partition->hauteur[clx] > partition->hauteur[cly])
        {
            partition->pere[cly] = clx;
        }
        else
        {
            partition->pere[cly] = clx;
            partition->hauteur[clx] = partition->hauteur[clx] + 1;
        }
    }
}

cellule *lister_classe(classe *partition, int x)
{
    cellule *tete = malloc(sizeof(cellule));
    tete->suiv=NULL;
    cellule* cour=tete;
    cellule *temp;
    for (int k = 0; k < partition->taille; k++)
    {
        if (recuperer_classe(partition, k) == x)
        {
            temp = malloc(sizeof(cellule));
            temp->num = k;
            temp->suiv = NULL;
            cour->suiv= temp;
            cour=temp;
        }
    }
    //cellule* stock=tete;  très crade, on ne libère pas stock...
    tete=tete->suiv;
    //free(stock);
    return tete;
}

tableauliste lister_partition(classe *partition)
{
    tableauliste liste;
    liste.taille = partition->taille;
    liste.data = malloc(liste.taille * sizeof(partition->taille));
    for (int k = 0; k < partition->taille; k++)
    {
        if (partition->pere[k] == k)
        {
            liste.data[k] = lister_classe(partition, k);
        }
        else
        {
            liste.data[k] = NULL;
        }
    }
    return liste;
}

void affiche(cellule *liste)
{
    if (liste)
    {
        cellule *cour = liste;
        while (cour)
        {
            printf("%d",cour->num);
            cour=cour->suiv;
        }
    }
    printf("\n");
}

int main()
{
    classe *partition = creer(11);
    fusion(partition, 0, 1);
    fusion(partition, 2, 3);
    fusion(partition, 10, 3);
    fusion(partition, 5, 9);
    fusion(partition, 4, 6);
    fusion(partition, 8, 7);
    fusion(partition, 7, 9);
    fusion(partition, 6, 8);
    tableauliste liste = lister_partition(partition);
    for (int k=0;k<partition->taille;k++)
    {
        affiche(liste.data[k]);
    }
}